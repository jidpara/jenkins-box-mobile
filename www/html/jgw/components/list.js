(function listFactory() {
  class List {
    constructor() {
      this.page = 1;
      this.pageCount = 10;

      this.renderItem = undefined;
      this.keyExtract = undefined;
      this.onRequest = undefined;

      this.onReqeustButtonClick = this.onReqeustButtonClick.bind(this);
      this.renderRequestButton = this.renderRequestButton.bind(this);
      this.mount = this.mount.bind(this);

      this.container = null;
      this.requestButton = null;
    }

    mount(renderList, renderItem, onRequest, keyExtract) {
      this.container = renderList();
      this.requestButton = this.renderRequestButton();
      this.container.after(this.requestButton);
      this.renderItem = renderItem;
      this.setOnRequest(onRequest);
      this.keyExtract = keyExtract;

      this.onRequest();
    }

    reload() {
      this.requestButton.style.visibility = "collapse";
      this.page = 1;
      if (typeof this.onRequest === "function") {
        this.onRequest();
      }
    }

    setOnRequest(onRequest) {
      this.onRequest = () => {
        onRequest(this.page, this.pageCount).then(({ list }) => {
          if (list !== undefined && Array.isArray(list)) {
            this.page = this.page + 1;
            if (list.length < this.pageCount) {
              this.requestButton.style.visibility = "collapse";
            } else {
              this.requestButton.style.visibility = "visible";
            }
            list.forEach((c) => this.renderItem(c));
          }
        });
      };
    }

    onReqeustButtonClick() {
      if (typeof this.onRequest === "function") {
        this.onRequest();
      }
    }

    renderRequestButton() {
      const button = document.createElement("a");
      button.classList.add("btn-list-more");
      button.classList.add("mgt30");
      button.style.visibility = "collapse";
      button.addEventListener("click", this.onReqeustButtonClick);

      const span = document.createElement("span");
      span.classList.add("text");
      span.textContent = "더 보기";

      button.append(span);

      return button;
    }
  }

  window.mountList = function mountList(renderList, renderItem, onRequest, getPageCount, keyExtract) {
    const list = new List();

    if (typeof renderList !== "function" || typeof renderItem !== "function" || typeof onRequest !== "function") {
      return;
    }

    if (getPageCount !== undefined && typeof getPageCount === "function") {
      list.pageCount = getPageCount();
    }

    list.mount(renderList, renderItem, onRequest, keyExtract);

    return list;
  };
})();
/**
 * 1. 데이터를 조회한다.
 * 2. 가져올 데이터가 더 있다.
 * 3. 더 보기 버튼을 표시힌다.
 * 4. 더 보기 버튼을 눌려 데이터를 조회한다.
 * 5. 가져올 데이터가 없다.
 * 6. 더 보기 버튼을 표시 하지 않는다.
 *
 * ON_REQUEST
 * REQUEST
 * UPDATE_DATA
 * KEY_EXTRACTOR
 * RENDER_ITEM
 *
 * renderList
 * renderItem
 * updateData
 * keyExtract
 * onRequest
 * getPageCount
 */
