(function pageFactory() {
  let zIndex = 0;
  class Page extends HTMLElement {
    constructor() {
      super();
      this.getTemplateAsync = this.getTemplateAsync.bind(this);
      this.fetchPageAsync = this.fetchPageAsync.bind(this);
      this.mountPageAsync = this.mountPageAsync.bind(this);
      this.openPageAsync = this.openPageAsync.bind(this);
      this.unmountPage = this.unmountPage.bind(this);
      this.closePage = this.closePage.bind(this);
      this.tryRelease = this.tryRelease.bind(this);
      this.reload = this.reload.bind(this);
      this.jQuerySelector = this.jQuerySelector.bind(this);
      this._openRoot = this._openRoot.bind(this);
      this._openPage = this._openPage.bind(this);
      this._bindReload = this._bindReload.bind(this);
      this._bindUnload = this._bindUnload.bind(this);
      this.releasePage = this.releasePage.bind(this);
      this.isForeground = this.isForeground.bind(this);
      this.functions = [];
      this.reloadFunctions = [];
      this.unloadFunctions = [];

      this.openOption = {
        root: "fadeIn",
        start: "fadeIn",
        menu: "slideInRight",
        chat_list: "slideInLeft",
        request_recieve_item_next: "slideInRight",
        request_recieve_item_prev: "slideInLeft",
        request_recieve_item_next_g: "slideInRight",
        request_recieve_item_prev_g: "slideInLeft",
      };

      this.closeOption = {
        menu: "slideOutRight",
        chat_list: "slideOutLeft",
        request_recieve_item_next: "slideOutRight",
        request_recieve_item_prev: "slideOutLeft",
        request_recieve_item_next_g: "slideOutRight",
        request_recieve_item_prev_g: "slideOutLeft",
      };

      this.releasePages = ["splash", "tutorial", "start", "menu"];

      this.parameter = undefined;

      this.futureReleasePages = [];
      // this.openPageAsync();
    }

    getTemplateAsync() {
      return new Promise((resolve, reject) => {
        const page = this.getAttribute("page");
        if (page === null) {
          reject("no page attribute.");
        }
        const template = document.getElementById(`template-${page}`);
        if (template === null) {
          this.fetchPageAsync(page).then(resolve).catch(reject);
        } else {
          resolve(template);
        }
      });
    }

    fetchPageAsync(page) {
      let html = `${page}.html`;
      if (window.device && window.device.platform === "iOS") {
        html = `/html/jgw/${html}`;
      }

      return axios(html).then(({ data }) => {
        const template = document.createElement("template");
        template.id = `template-${page}`;
        template.innerHTML = data;
        const container = document.body.getElementsByTagName("template-container")[0];
        container.appendChild(template);
        return template;
      });
    }

    mountPageAsync() {
      return this.getTemplateAsync().then((template) => {
        const shadow = this.attachShadow({ mode: "open" });

        const mainlinkElem = document.createElement("link");
        mainlinkElem.setAttribute("rel", "stylesheet");
        mainlinkElem.setAttribute("href", "css/main.css");
        shadow.appendChild(mainlinkElem);

        const swipelinkElem = document.createElement("link");
        swipelinkElem.setAttribute("rel", "stylesheet");
        swipelinkElem.setAttribute("href", "css/swiper.min.css");
        shadow.appendChild(swipelinkElem);

        const content = template.content;
        Array.from(content.children).forEach((child) => {
          this.functions.push(() => this._ui(this.jQuerySelector));
          if (child.tagName.toLowerCase() === "script") {
            const diFunctionFactory = new Function(
              `return (function($, GET_PARAMETER, RELOAD, UNLOAD, GET_THIS, IS_FOREGROUND){
                ${child.innerHTML} 
                try{list = window.mountList(renderList, renderItem, onRequest, getPageCount)}catch{}
              });`
            );

            const diFunction = diFunctionFactory();
            this.functions.push(() =>
              diFunction(
                this.jQuerySelector,
                () => window.getParameter(this.parameter),
                this._bindReload,
                this._bindUnload,
                () => this,
                this.isForeground
              )
            );
            return;
          }
          shadow.appendChild(child.cloneNode(true));
        });
      });
    }

    unmountPage() {}

    openPageAsync(_parameter) {
      this.parameter = _parameter;
      return this.mountPageAsync().then(() => {
        const container = document.getElementsByTagName("page-container")[0];
        Array.from(container.children).forEach((child) => {
          const _page = child.getAttribute("page");
          if (this.releasePages.includes(_page)) {
            this.futureReleasePages.push(child);
          }
        });

        const page = this.getAttribute("page");
        if (page === "splash") {
          this._openRoot(container);
        } else {
          this._openPage(container);
        }
      });
    }

    _openRoot(container) {
      container.appendChild(this);
      this.functions.forEach((f) => f());
      this.futureReleasePages.forEach((_page) => {
        container.removeChild(_page);
      });
    }

    _openPage(container) {
      zIndex += 100;
      const page = this.getAttribute("page");
      const animation = this.openOption[page] || "slideInUp";
      this.classList.add("animated", animation, "faster");
      this.addEventListener("animationend", () => {
        this.futureReleasePages.forEach((_page) => {
          if (_page.parentNode) {
            _page.parentNode.removeChild(_page);
          }
        });
      });
      this.style.zIndex = zIndex;
      container.appendChild(this);
      this.functions.forEach((f) => f());
    }

    releasePage() {
      const container = document.getElementsByTagName("page-container")[0];
      if (Array.from(container.children).includes(this)) {
        container.removeChild(this);
      }
      this.remove();
      this.unloadFunctions.forEach((f) => f());
    }

    closePage() {
      return new Promise((resolve) => {
        const page = this.getAttribute("page");
        // const animation = this.openOption[page] || 'slideInUp';
        const animation = this.closeOption[page] || "slideOutDown";
        this.className = "";
        this.classList.add("animated", animation, "faster");
        this.addEventListener(
          "animationend",
          () => {
            this.releasePage();
            resolve();
          },
          { once: true }
        );
      });
    }

    tryRelease() {
      const page = this.getAttribute("page");
      if (this.releasePages.includes(page)) {
        this.releasePage();
      }
    }

    reload(parameter) {
      this.reloadFunctions.forEach((f) => f(parameter));
    }

    _bindReload(reloadFunction) {
      this.reloadFunctions.push(reloadFunction);
    }

    _bindUnload(unloadFunction) {
      this.unloadFunctions.push(unloadFunction);
    }

    _bindObserver() {
      this.unloadFunctions.push(unloadFunction);
    }

    _ui($) {
      // $('.js-tab a').on('click', function(e) {
      //     e.preventDefault();
      //     $(this)
      //         .closest('.js-tab')
      //         .find('a')
      //         .removeClass('on');
      //     $(this).addClass('on');
      //     let index = $(this)
      //         .closest('.js-tab')
      //         .find('a')
      //         .index(this);
      //     $(this)
      //         .closest('.js-tab')
      //         .next()
      //         .find('.js-tab-item')
      //         .removeClass('on');
      //     $(this)
      //         .closest('.js-tab')
      //         .next()
      //         .find('.js-tab-item')
      //         .eq(index)
      //         .addClass('on');
      // });
      // let btnAccordion = $('.js-accordion .btn-accordion');
      // btnAccordion.eq(0).trigger('click');
      // btnAccordion.on('click', function() {
      //     $(this).toggleClass('on');
      //     btnAccordion.not(this).removeClass('on');
      //     return false;
      // });
      // btnAccordion.eq(0).trigger('click');
      // $('.toggle').on('click', function(e) {
      //     e.preventDefault();
      //     $(this).toggleClass('on');
      // });
      // $('textarea.js-auto').on('keydown keyup', function() {
      //     $(this)
      //         .height(1)
      //         .height($(this).prop('scrollHeight') + 12);
      // });
      // let posY;
      // $('.btn-sort').on('click', function(e) {
      //     e.preventDefault();
      //     // posY = $(window).scrollTop();   //no shadow dom case
      //     // $('html, body').addClass('scroll-fixed');
      //     // $('.wrap').css({'position': 'absolute', 'width': '100%', 'top': -posY});
      //     // $('.header-top').css({'top': 0});
      //     $('.sheet-wrap, .bg-dim').addClass('open');
      // });
      // $('.bg-dim').on('click', function() {
      //     // $('html, body').removeClass('scroll-fixed'); //no shadow dom case
      //     // posY = $(window).scrollTop(posY);
      //     $('.wrap, .header-top').removeAttr('style');
      //     $('.sheet-wrap, .bg-dim').removeClass('open');
      // });
      // $('.btn-sheet').on('click', function(e) {
      //     e.preventDefault();
      //     if (!$(this).hasClass('cancel')) {
      //         $('.btn-sheet').removeClass('active');
      //         $(this).addClass('active');
      //         $('.btn-sort span').text(
      //             $(this)
      //                 .find('span')
      //                 .text()
      //         );
      //     }
      //     $('.bg-dim').trigger('click');
      // });

      // SWIPER UPDATE 공통 처리
      setTimeout(function () {
        $(".swiper-wrapper").each(function (index, item) {
          // 대상 node 선택
          const target = $(this).get(0);

          let slideMedium;
          let slideSmall;
          let swiperWide;

          if ($(this).parent().hasClass("slide-small")) {
            slideSmall = new Swiper($(this).closest(".slide-small"), {
              slidesPerView: "auto",
              spaceBetween: 10,
              slidesOffsetBefore: 14,
              slidesOffsetAfter: 14,
            });
          }

          if ($(this).parent().hasClass("slide-medium")) {
            slideMedium = new Swiper($(this).closest(".slide-medium"), {
              slidesPerView: "auto",
              spaceBetween: 10,
              slidesOffsetBefore: 14,
              slidesOffsetAfter: 14,
            });
          }

          if ($(this).parent().hasClass("slide-wide")) {
            swiperWide = new Swiper($(this).closest(".slide-wide"), {
              pagination: {
                el: ".swiper-pagination",
              },
            });
          }

          // const slideSmall = new Swiper(`${$(this).get}.slide-small`, {
          //   slidesPerView: "auto",
          //   spaceBetween: 10,
          //   slidesOffsetBefore: 14,
          //   slidesOffsetAfter: 14,
          // });

          if (target instanceof Element || target instanceof HTMLDocument) {
            // CHILD NODE 변경 시 이벤트
            const observer = new MutationObserver(function (mutations) {
              mutations.forEach(function (mutation) {
                console.log(mutation.type);
                if (mutation.type === "childList") {
                  if (slideSmall) {
                    slideSmall.update();
                  }
                  if (slideMedium) {
                    slideSmall.update();
                  }
                  if (swiperWide) {
                    swiperWide.update();
                  }
                }
              });
            });

            // 감시자 옵션 포함, 대상 노드에 전달
            observer.observe(target, { childList: true });
          }
        });
      }, 300);
    }

    jQuerySelector(selector) {
      return $(selector, this.shadowRoot);
    }

    isForeground() {
      const container = document.getElementsByTagName("page-container")[0];
      return container.lastChild === this;
    }
  }
  customElements.define("app-page", Page);
})();
