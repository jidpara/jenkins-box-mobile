(function pictureFactory() {
  const PHOTOLIBRARY = 0;
  const CAMERA = 1;
  const SAVEDPHOTOALBUM = 2;

  class Picture extends HTMLElement {
    constructor() {
      super();

      this.open = this.open.bind(this);
      this.close = this.close.bind(this);
      this.gallery = this.gallery.bind(this);
      this.camera = this.camera.bind(this);
      this.video = this.video.bind(this);
      this.dispatchPictureEvent = this.dispatchPictureEvent.bind(this);
      this.dispatchPictureEventMulti = this.dispatchPictureEventMulti.bind(this);

      const container = document.createElement("section");
      container.className = "container";
      container.style.zIndex = 100 * 100 * 100 + 1;
      this.appendChild(container);

      const contents = document.createElement("article");
      contents.className = "contents";
      container.appendChild(contents);

      this.sheetWrap = document.createElement("div");
      this.sheetWrap.className = "sheet-wrap picture";
      contents.appendChild(this.sheetWrap);

      const sheetHead = document.createElement("div");
      sheetHead.className = "sheet-header";
      this.sheetWrap.appendChild(sheetHead);

      const sheetHeadText = document.createElement("span");
      sheetHeadText.className = "text title";
      sheetHeadText.textContent = "작업선택";
      sheetHead.appendChild(sheetHeadText);

      const closeTrigger = document.createElement("a");
      closeTrigger.className = "btn-close-large";
      closeTrigger.addEventListener("click", this.close);
      sheetHead.appendChild(closeTrigger);

      const closeLabel = document.createElement("span");
      closeLabel.textContent = "닫기";
      closeTrigger.appendChild(closeLabel);

      const sheetIconWrap = document.createElement("div");
      sheetIconWrap.className = "sheet-icon-wrap";
      this.sheetWrap.appendChild(sheetIconWrap);

      const galleryButton = document.createElement("a");
      galleryButton.className = "btn-picture";
      galleryButton.addEventListener("click", this.gallery);
      sheetIconWrap.appendChild(galleryButton);

      const galleryButtonIcon = document.createElement("img");
      galleryButtonIcon.src = "./images/icon/picture.png";
      galleryButton.appendChild(galleryButtonIcon);

      const galleryButtonText = document.createElement("span");
      galleryButtonText.className = "text title";
      galleryButtonText.textContent = "사진";
      galleryButton.appendChild(galleryButtonText);

      // 카메라
      const cameraButton = document.createElement("a");
      cameraButton.className = "btn-camera";
      cameraButton.addEventListener("click", this.camera);
      sheetIconWrap.appendChild(cameraButton);

      const cameraButtonIcon = document.createElement("img");
      cameraButtonIcon.src = "./images/icon/camera-p-r.png";
      cameraButton.appendChild(cameraButtonIcon);

      const cameraButtonText = document.createElement("span");
      cameraButtonText.className = "text title";
      cameraButtonText.textContent = "카메라";
      cameraButton.appendChild(cameraButtonText);

      // 비디오
      const videoButton = document.createElement("a");
      videoButton.className = "btn-camera";
      videoButton.addEventListener("click", this.video);
      sheetIconWrap.appendChild(videoButton);

      const videoButtonIcon = document.createElement("img");
      videoButtonIcon.src = "./images/icon/video.png";
      videoButton.appendChild(videoButtonIcon);

      const videoButtonText = document.createElement("span");
      videoButtonText.className = "text title";
      videoButtonText.textContent = "동영상";
      videoButton.appendChild(videoButtonText);

      this.fileButton = document.createElement("label");
      this.fileButton.className = "btn-picture";
      this.fileButton.setAttribute("for", "app-picture-file-input");
      sheetIconWrap.appendChild(this.fileButton);

      const fileDummy = document.createElement("a");
      this.fileButton.appendChild(fileDummy);

      const fileInput = document.createElement("input");
      fileInput.setAttribute("id", "app-picture-file-input");
      fileInput.setAttribute("type", "file");
      fileInput.style.display = "none";
      fileInput.addEventListener("change", this.fileOpen);
      fileDummy.appendChild(fileInput);

      if (window.ENV.PLATFORM !== "android") {
        const fileButtonIcon = document.createElement("img");
        fileButtonIcon.src = "./images/icon/file.png";
        fileDummy.appendChild(fileButtonIcon);

        const fileButtonText = document.createElement("span");
        fileButtonText.className = "text title";
        fileButtonText.textContent = "파일";
        fileDummy.appendChild(fileButtonText);
      }

      this.dim = document.createElement("div");
      this.dim.classList.add("bg-dim");
      this.dim.style.zIndex = 100 * 100 * 100;
      container.addEventListener("click", this.close);
      this.appendChild(this.dim);

      this.resolveProxy = null;

      window.picture = this.open;
    }

    open() {
      if ($(this).hasClass("open")) {
        return;
      }
      this.dim.classList.add("open");
      this.sheetWrap.classList.add("open");
      this.classList.add("open");
    }

    close() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.sheetWrap.classList.remove("open");
    }

    gallery() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.sheetWrap.classList.remove("open");
      console.log("gallery");
      // if (navigator.camera) {
      //   navigator.camera.getPicture(this.dispatchPictureEvent, (e) => console.error("app-picture", e), {
      //     sourceType: PHOTOLIBRARY,
      //     saveToPhotoAlbum: true,
      //     targetHeight: 1024,
      //     targetWidth: 1024,
      //   });
      // }
      window.imagePicker.getPictures(this.dispatchPictureEventMulti, (e) => console.error("app-picture", e), {
        maximumImagesCount: 4,
        width: 1024,
      });
    }

    camera() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.sheetWrap.classList.remove("open");
      console.log("camera");
      if (navigator.camera) {
        navigator.camera.getPicture(this.dispatchPictureEvent, (e) => console.error("app-picture", e), {
          sourceType: CAMERA,
          targetHeight: 1024,
          targetWidth: 1024,
          correctOrientation: true,
        });
      }
    }

    video() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.sheetWrap.classList.remove("open");
      console.log("video");
      if (navigator.camera) {
        navigator.camera.getPicture(this.dispatchPictureEvent, (e) => console.error("app-picture", e), {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          mediaType: Camera.MediaType.VIDEO,
        });
      }
    }

    fileOpen(e) {
      if (e.target && e.target.files && Array.from(e.target.files).length > 0) {
        const file = Array.from(e.target.files)[0];
        const fileName = file.name;
        window.dispatchEvent(
          new CustomEvent("picture", {
            detail: { file, fileName },
          })
        );
        e.target.value = "";
      }
    }

    dispatchPictureEvent(imgUri) {
      // 영상업로드 시 file:/// 이 안붙어서 내려오는 현상이 있음
      const replaceURI = imgUri.indexOf("file:") > -1 ? imgUri : "file:///" + imgUri;
      if (typeof window.resolveLocalFileSystemURL === "function") {
        window.resolveLocalFileSystemURL(
          replaceURI,
          function success(fileEntry) {
            fileEntry.file((file) => {
              // window.dispatchEvent(new CustomEvent("picture", { detail: { file } }));
              let fileName = file.name;
              if (
                fileName === undefined ||
                fileName === null ||
                typeof fileName.indexOf !== "function" ||
                fileName.indexOf(".") === -1
              ) {
                try {
                  if (file && file.type && file.type.indexOf("/") !== -1) {
                    fileName = "filename." + file.type.substring(file.type.indexOf("/") + 1);
                  }
                } catch (e) {
                  console.error("Parsing file type to extension Error", file);
                }
              }
              const reader = new FileReader();
              reader.onloadend = () => {
                window.dispatchEvent(
                  new CustomEvent("picture", {
                    detail: { file: new Blob([new Uint8Array(reader.result)], { type: file.type }), fileName },
                  })
                );
              };
              reader.readAsArrayBuffer(file);
            });
          },
          (e) => console.error("app-picture", e)
        );
      }
    }

    dispatchPictureEventMulti(results) {
      results.forEach((item) => {
        this.dispatchPictureEvent(item);
      });
    }
  }
  customElements.define("app-picture", Picture);
})();
