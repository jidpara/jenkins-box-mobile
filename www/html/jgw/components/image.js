(function imageFactory() {
  class AppImage extends HTMLElement {
    // static get observedAttributes() {
    //   return ["src", "alt-src", "default-src"];
    // }

    constructor() {
      super();
      this.coreImg = null;
      this.STAGE = null;
      this.trySetDelay = this.trySetDelay.bind(this);
      this.setSource = this.setSource.bind(this);
      this.onCoreImgError = this.onCoreImgError.bind(this);
      this.coreImg = document.createElement("img");
      this.coreImg.addEventListener("error", this.onCoreImgError);
      // 클래스는 코어 img 로 복사한다.
      Array.from(this.classList || []).forEach((c) => this.coreImg.clasList.add(c));
    }

    trySetDelay() {
      if ("IntersectionObserver" in window) {
        let lazyImageObserver = new IntersectionObserver((entries) => {
          entries.forEach((entry) => {
            if (entry.isIntersecting) {
              this.setSource();
              lazyImageObserver.unobserve(this);
            }
          });
        });
        lazyImageObserver.observe(this);
      } else {
        this.setSource();
      }
    }

    onCoreImgError() {
      let alt = this.getAttribute("alt-src");
      let dft = this.getAttribute("default-src");
      // TODO : MIME TYPE PARSE
      if (
        alt &&
        alt !== null &&
        (alt.includes(".avi") || alt.includes(".mpeg") || alt.includes(".mpg") || alt.includes(".mp4"))
      ) {
        alt = "./images/icon/default-video.png";
        dft = "./images/icon/default-video.png";
      }
      if (this.STAGE === "src") {
        this.STAGE = "alt";
        this.coreImg.setAttribute("src", alt);
      } else if (this.STAGE === "alt") {
        this.STAGE = "dft";
        if (dft !== null) {
          this.coreImg.setAttribute("src", dft);
        } else {
          this.coreImg.style.visibility = "hidden";
        }
      }
    }

    setSource() {
      this.coreImg.style.visibility = "visible";
      const src = this.getAttribute("src");
      this.STAGE = "src";
      this.coreImg.setAttribute("src", src);
    }

    connectedCallback() {
      this.style.width = "100%";
      this.style.height = "100%";
      this.append(this.coreImg);
      this.trySetDelay();
      //   this.setSource();
    }

    // attributeChangedCallback() {
    //   this.setSource();
    // }
  }
  customElements.define("app-img", AppImage);
})();
