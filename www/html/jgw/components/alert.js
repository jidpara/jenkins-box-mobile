(function alertFactory() {
  class Alert extends HTMLElement {
    constructor() {
      super();

      this.open = this.open.bind(this);
      this.close = this.close.bind(this);
      this.done = this.done.bind(this);
      this.getCateogory = this.getCateogory.bind(this);

      const container = document.createElement("section");
      container.className = "container";
      container.style.zIndex = 100 * 100 * 100 + 1;
      this.appendChild(container);

      const contents = document.createElement("article");
      contents.className = "contents";
      container.appendChild(contents);

      this.popWrap = document.createElement("div");
      this.popWrap.className = "pop-wrap";
      contents.appendChild(this.popWrap);

      const popHead = document.createElement("div");
      popHead.className = "pop-head";
      this.popWrap.appendChild(popHead);

      const closeTrigger = document.createElement("a");
      closeTrigger.className = "btn-close-large";
      closeTrigger.addEventListener("click", this.close);
      popHead.appendChild(closeTrigger);

      const closeLabel = document.createElement("span");
      closeLabel.textContent = "닫기";
      closeTrigger.appendChild(closeLabel);

      const popBody = document.createElement("div");
      popBody.className = "pop-body";
      popBody.classList.add("default");
      this.popWrap.appendChild(popBody);

      this.titleContainer = document.createElement("h2");
      popBody.appendChild(this.titleContainer);

      this.messageContainer = document.createElement("p");
      popBody.appendChild(this.messageContainer);

      this.categoriesContainer = document.createElement("ul");
      this.categoriesContainer.className = "category-set";
      this.categoriesContainer.style.display = "none";
      popBody.appendChild(this.categoriesContainer);

      const popBtnWrap = document.createElement("div");
      popBtnWrap.className = "pop-btn-wrap";
      this.popWrap.appendChild(popBtnWrap);

      this.closeBottomTrigger = document.createElement("a");
      this.closeBottomTrigger.className = "btn-pop-confirm";
      this.closeBottomTrigger.addEventListener("click", this.done);
      popBtnWrap.appendChild(this.closeBottomTrigger);

      const closeBottomLabel = document.createElement("span");
      closeBottomLabel.className = "text";
      closeBottomLabel.textContent = "확인";
      this.closeBottomTrigger.appendChild(closeBottomLabel);

      this.checkBoxContainer = document.createElement("label");
      this.checkBoxContainer.style.visibility = "collapse";
      this.popWrap.appendChild(this.checkBoxContainer);

      this.checkBoxButton = document.createElement("input");
      this.checkBoxButton.setAttribute("type", "checkbox");
      this.checkBoxButton.className = "shape-7";
      this.checkBoxContainer.appendChild(this.checkBoxButton);

      this.checkBoxText = document.createElement("span");
      this.checkBoxText.className = "text";
      this.checkBoxContainer.appendChild(this.checkBoxText);

      this.dim = document.createElement("div");
      this.dim.classList.add("bg-dim");
      this.dim.style.zIndex = 100 * 100 * 100;
      this.dim.addEventListener("click", this.close);
      this.appendChild(this.dim);

      this.cateSeq = null;
      this.cateName = null;

      this.resolveProxy = null;

      window.Alert = this.open;
    }

    renderCancel() {
      const cancel = document.createElement("li");
      const cancelButton = document.createElement("a");
      cancelButton.className = "btn-sheet cancel";
      const cancelText = document.createElement("span");
      cancelText.className = "text";
      cancelText.innerText = "취소";
      cancel.appendChild(cancelButton);
      cancelButton.appendChild(cancelText);
      cancelButton.addEventListener("click", this.cancel);
      return cancel;
    }

    open(title, message, option = {}) {
      if ($(this).hasClass("open")) {
        return;
      }
      this.titleContainer.textContent = title;
      this.messageContainer.innerHTML = message;
      this.dim.classList.add("open");
      this.popWrap.classList.add("open");
      this.classList.add("open");

      if (option.pre === true) {
        this.messageContainer.style.whiteSpace = "pre-line";
      }

      this.closeBottomTrigger.className = "btn-pop-confirm";
      if (option.color) {
        this.closeBottomTrigger.classList.add(option.color);
      }

      this.checkBoxContainer.style.visibility = "collapse";
      if (option.checkbox) {
        this.checkBoxButton.checked = false;
        this.checkBoxText.innerHTML = option.checkbox;
        this.checkBoxContainer.style.visibility = "visible";
      }

      if (option.toast) {
        const { duration } = option.toast;
        setTimeout(() => {
          $(this).fadeOut(1000);
          this.close();
        }, duration || 1000);
      }

      this.categoriesContainer.style.display = "none";
      this.categoriesContainer.innerHTML = "";
      if (option.categories) {
        let categoryArray =
          (Array.isArray(option.categories) && option.categories) ||
          Array.from(
            (window.member && window.member.shopInfoResponse && window.member.shopInfoResponse.shopCategoryResponse) ||
              []
          ).map(({ cateSeq, cateName }) => ({ cateSeq1dp: cateSeq, cateSeq1dpName: cateName }));
        for (let i = 0; i < categoryArray.length; i++) {
          const { cateSeq1dp, cateSeq1dpName } = categoryArray[i];
          const categoryCheck = $(`
            <li>
              <a>
                <label id="category-check-label-${i}" for="category-check-${i}" class="category-check">
                    <input type="radio" id="category-check-${i}" name="category-check" value="${cateSeq1dp}" data-name="${cateSeq1dpName}">
                    <span class="form-txt text" is="c-span">${cateSeq1dpName}</span>
                </label>
              </a>
            </li>`);
          $(this.categoriesContainer).append(categoryCheck);
        }
        $("#category-check-0").prop("checked", true);
        $("#category-check-label-0").addClass("checked");
        const inputSelector = $("input", this.categoriesContainer);
        inputSelector.change(function () {
          $(this).parent().toggleClass("checked", true);
          inputSelector.each((_, el) => {
            if (el !== this) {
              $(el).parent().removeClass("checked");
            }
          });
        });
        this.categoriesContainer.style.display = "flex";
      }

      return new Promise((resolve) => {
        this.resolveProxy = resolve;
      });
    }

    getCateogory() {
      if ($(":checked", this.categoriesContainer).length > 0) {
        const cateSeq1dp = $(":checked", this.categoriesContainer).val();
        const title = $(":checked", this.categoriesContainer).data("name");
        return { cateSeq1dp, title };
      }
    }

    close() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.popWrap.classList.remove("open");
      if (this.resolveProxy !== null) {
        this.resolveProxy({ cause: "clsoe", category: this.getCateogory() });
      }
    }

    done() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.popWrap.classList.remove("open");
      if (this.resolveProxy !== null) {
        this.resolveProxy({
          cause: "done",
          checkbox: this.checkBoxButton.checked,
          category: this.getCateogory(),
        });
      }
    }
  }
  customElements.define("app-alert", Alert);
})();
