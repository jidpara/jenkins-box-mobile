(function calendarFactory() {
    class Calendar extends HTMLElement {
        constructor() {
            super();

            this.open = this.open.bind(this);

            const container = document.createElement("div");

            this.input = document.createElement("input")
            this.style.display = "none";
            this.input.setAttribute("type", "date")

            container.appendChild(this.input)
            document.body.appendChild(container)

            window.calendar = {
                open: this.open
            }

            $(document).ready(() => {
                const onClose = (done) => {
                    if (done === true) {
                        window.dispatchEvent(
                            new CustomEvent("calendar_select", {
                                detail: {
                                    value: this.instance.toString(),
                                },
                            })
                        );
                    }
                }
                window.M.Datepicker.init(this.input, { format: "yyyy-mm-dd", formatSubmit: "yyyy-mm-dd", setDefaultDate: true, onClose });
                this.instance = window.M.Datepicker.getInstance(this.input);
            })
        }


        open(value) {
            let nextValue = new Date();
            if (value !== '') {
                nextValue = new Date(value);
            }
            this.instance.setDate(nextValue);
            this.instance.open();
        }
    }
    customElements.define("app-calendar", Calendar);
})();
