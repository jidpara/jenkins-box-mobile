(function headerFactory() {
  class Header extends HTMLElement {
    static get observedAttributes() {
      return ["title", "close", "root"];
    }

    constructor() {
      super();

      this.currentBackAction = null;
      this.currentTrashAction = null;
      this.currentLeftTextAction = null;
      this.currentRightTextAction = null;

      const shadow = this.attachShadow({ mode: "open" });

      // Apply external styles to the shadow dom
      const linkElem = document.createElement("link");
      linkElem.setAttribute("rel", "stylesheet");
      linkElem.setAttribute("href", "css/main.css");

      shadow.appendChild(linkElem);

      const template = document.getElementById("template-header");
      const templateContent = template.content;

      shadow.appendChild(templateContent.cloneNode(true));

      this.updateTitle = this.updateTitle.bind(this);
      this.updateClose = this.updateClose.bind(this);
      this.updateRoot = this.updateRoot.bind(this);
      this.back = this.back.bind(this);
      this.trash = this.trash.bind(this);

      const navigateToBack = window.getNavigateBack();
      if (navigateToBack !== undefined) {
        this.shadowRoot.getElementById("back").classList.remove("hidden");
        this.back(() => navigateToBack());
      }
      this.shadowRoot.getElementById("close").addEventListener("click", window.navigateToRoot);
      this.shadowRoot.getElementById("chat").addEventListener("click", () => navigate("chat_list"));
      this.shadowRoot.getElementById("alarm").addEventListener("click", () => navigate("alarm_list"));
      this.shadowRoot.getElementById("menu").addEventListener("click", () => navigate("menu"));

      const root = this.getAttribute("root");
      if (root !== null) {
        this.updateRoot();
      }
    }

    title(nextTitle) {
      if (nextTitle === undefined) {
        return this.getAttribute("title");
      }
      this.setAttribute("title", nextTitle);
    }

    connectedCallback() {
      const title = this.getAttribute("title");
      this.updateTitle(title);
      const close = this.getAttribute("close");
      this.updateClose(close);
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (oldValue !== newValue) {
        if (name === "title") {
          this.updateTitle(newValue);
        }
        if (name === "close") {
          this.updateClose(newValue);
        }
      }
    }

    updateTitle(title) {
      this.shadowRoot.getElementById("title").innerHTML = title;
    }

    updateClose(close) {
      if (close === true && close === "true") {
        this.shadowRoot.getElementById("close").classList.remove("hidden");
      } else {
        this.shadowRoot.getElementById("close").classList.add("hidden");
      }
    }

    updateRoot() {
      this.shadowRoot.getElementById("title").classList.add("logo");
      this.shadowRoot.getElementById("alarm").classList.remove("hidden");
      this.shadowRoot.getElementById("chat").classList.remove("hidden");
      this.shadowRoot.getElementById("menu").classList.remove("hidden");

      if (window.root !== undefined) {
        if (window.root.alarmCnt > 0) {
          $(this.shadowRoot.getElementById("alarmCnt")).show();
        } else {
          $(this.shadowRoot.getElementById("alarmCnt")).hide();
        }
        if (window.root.chatCnt > 0) {
          $(this.shadowRoot.getElementById("chatCnt")).show();
        } else {
          $(this.shadowRoot.getElementById("chatCnt")).hide();
        }
      }
    }

    back(action) {
      if (action === false) {
        this.shadowRoot.getElementById("back").classList.add("hidden");
        return;
      }
      if (this.currentBackAction !== null) {
        this.shadowRoot.getElementById("back").removeEventListener("click", this.currentBackAction);
      }
      this.shadowRoot.getElementById("back").addEventListener("click", action);
      this.shadowRoot.getElementById("back").classList.remove("hidden");
      this.currentBackAction = action;
    }

    trash(action) {
      if (this.currentTrashAction !== null) {
        this.shadowRoot.getElementById("trash").removeEventListener("click", this.currentTrashAction);
      }
      this.shadowRoot.getElementById("trash").classList.remove("hidden");
      this.shadowRoot.getElementById("trash").addEventListener("click", action);
      this.currentTrashAction = action;
    }

    leftText(action, text) {
      if (action === false) {
        this.shadowRoot.getElementById("leftText").classList.add("hidden");
        return;
      }
      if (this.currentLeftTextAction !== null) {
        this.shadowRoot.getElementById("leftText").removeEventListener("click", this.currentLeftTextAction);
      }
      this.shadowRoot.getElementById("leftText").classList.remove("hidden");
      this.shadowRoot.getElementById("leftText").addEventListener("click", action);
      this.shadowRoot.getElementById("leftTextTitle").textContent = text;
      this.currentLeftTextAction = action;
    }

    leftCancel(action, text) {
      if (action === false) {
        this.shadowRoot.getElementById("leftCancel").classList.add("hidden");
        return;
      }
      if (this.currentLeftTextAction !== null) {
        this.shadowRoot.getElementById("leftCancel").removeEventListener("click", this.currentLeftTextAction);
      }
      this.shadowRoot.getElementById("leftCancel").classList.remove("hidden");
      this.shadowRoot.getElementById("leftCancel").addEventListener("click", action);
      this.shadowRoot.getElementById("leftCancelTitle").textContent = text;
      this.currentLeftTextAction = action;
    }

    rightText(action, text) {
      if (action === false) {
        this.shadowRoot.getElementById("rightText").classList.add("hidden");
        return;
      }
      if (this.currentRightTextAction !== null) {
        this.shadowRoot.getElementById("rightText").removeEventListener("click", this.currentRightTextAction);
      }
      this.shadowRoot.getElementById("rightText").classList.remove("hidden");
      this.shadowRoot.getElementById("rightText").addEventListener("click", action);
      this.shadowRoot.getElementById("rightTextTitle").textContent = text;
      this.currentRightTextAction = action;
    }

    onSearch(action) {
      const search_input = this.shadowRoot.getElementById("search_input");
      search_input.addEventListener("search", action);
      this.shadowRoot.getElementById("search_clear").addEventListener("click", () => (search_input.value = ""));
      this.shadowRoot.getElementById("search_container").classList.remove("hidden");
      setTimeout(() => {
        search_input.focus();
      }, 1000);
    }

    setSearch(text) {
      this.shadowRoot.getElementById("search_input").value = text;
    }
  }
  // header 등록
  document.addEventListener(
    "deviceready",
    () => {
      let html = "components/header.html";
      if (window.device && window.device.platform === "iOS") {
        html = `/html/jgw/${html}`;
      }

      axios(html)
        .then(({ data }) => {
          const template = document.createElement("template");
          template.id = "template-header";
          template.innerHTML = data;
          const container = document.getElementsByTagName("template-container")[0];
          container.appendChild(template);
        })
        .then(() => {
          customElements.define("app-header", Header);
        });
    },
    false
  );
})();
