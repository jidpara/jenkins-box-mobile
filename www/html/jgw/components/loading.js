(function loadingFactory() {
  class Loading extends HTMLElement {
    constructor() {
      super();

      this.open = this.open.bind(this);
      this.close = this.close.bind(this);

      const container = document.createElement("section");
      container.className = "container";
      container.style.zIndex = 100 * 100 * 100 + 11;
      this.appendChild(container);

      const contents = document.createElement("article");
      contents.className = "contents";
      container.appendChild(contents);

      this.popWrap = document.createElement("div");
      this.popWrap.className = "pop-wrap back-none";
      contents.appendChild(this.popWrap);

      const ldsDefault = document.createElement("div");
      ldsDefault.className = "lds-default";
      this.popWrap.appendChild(ldsDefault);

      for (let i = 0; i < 12; i++) {
        const box = document.createElement("div");
        ldsDefault.appendChild(box);
      }

      this.dim = document.createElement("div");
      this.dim.classList.add("bg-dim");
      this.dim.style.zIndex = 100 * 100 * 100 + 10;
      this.appendChild(this.dim);

      // window.Alert = this.open;
      window.showLoading = this.open;
      window.hideLoading = this.close;
    }

    open() {
      if ($(this).hasClass("open")) {
        return;
      }
      this.dim.classList.add("open");
      this.popWrap.classList.add("open");
      this.classList.add("open");
    }

    close() {
      this.classList.remove("open");
      this.dim.classList.remove("open");
      this.popWrap.classList.remove("open");
    }
  }
  customElements.define("app-loading", Loading);
})();
