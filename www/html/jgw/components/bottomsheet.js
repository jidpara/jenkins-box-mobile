(function bottomSheetFactory() {
  class BottomSheet extends HTMLElement {
    constructor() {
      super();

      this.renderCancel = this.renderCancel.bind(this);
      this.open = this.open.bind(this);
      this.renderItem = this.renderItem.bind(this);
      this.select = this.select.bind(this);
      this.cancel = this.cancel.bind(this);

      this.dim = document.createElement("div");
      this.dim.classList.add("bg-dim");
      this.dim.style.zIndex = 100 * 100 * 100;
      this.dim.addEventListener("click", this.cancel);
      this.appendChild(this.dim);

      this.ul = document.createElement("ul");
      this.ul.className = "sheet-wrap";
      this.ul.style.zIndex = 100 * 100 * 100 + 1;
      this.appendChild(this.ul);
    }

    renderCancel() {
      const cancel = document.createElement("li");
      const cancelButton = document.createElement("a");
      cancelButton.className = "btn-sheet cancel";
      const cancelText = document.createElement("span");
      cancelText.className = "text";
      cancelText.innerText = "취소";
      cancel.appendChild(cancelButton);
      cancelButton.appendChild(cancelText);
      cancelButton.addEventListener("click", this.cancel);
      return cancel;
    }

    open(items, value) {
      if ($(this.ul).hasClass("open")) {
        return;
      }
      this.ul.innerHTML = "";
      Array.from(items).forEach((item) => {
        this.ul.appendChild(this.renderItem(item, value));
      });
      this.ul.appendChild(this.renderCancel());
      this.dim.classList.add("open");
      this.ul.classList.add("open");
    }

    renderItem(item, value) {
      const li = document.createElement("li");
      const button = document.createElement("a");
      button.className = "btn-sheet";
      const text = document.createElement("span");
      text.className = "text";
      text.innerText = item.text;
      li.appendChild(button);
      button.appendChild(text);
      button.addEventListener("click", () => {
        this.select(item);
        this.dim.classList.remove("open");
        this.ul.classList.remove("open");
      });
      if (item.value === value) {
        button.classList.add("active");
      }
      return li;
    }

    select(item) {
      this.dispatchEvent(
        new CustomEvent("select", {
          detail: {
            value: item.value,
            data: item,
          },
        })
      );
      this.ul.classList.remove("open");
    }

    cancel() {
      this.dim.classList.remove("open");
      this.ul.classList.remove("open");
    }
  }
  customElements.define("app-bottom-sheet", BottomSheet);
})();
