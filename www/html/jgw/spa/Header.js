(function headerFactory() {
  class Header extends HTMLElement {
    static get observedAttributes() {
      return ["title"];
    }

    constructor() {
      super();

      const template = document.getElementById("template-header");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));

      this.updateTitle = this.updateTitle.bind(this);
      this.shadowRoot.getElementById("back").setAttribute("onclick", window.navigateToBack);
      // this.updateBackEvent = this.updateBackEvent.bind(this);
    }

    connectedCallback() {
      const title = this.getAttribute("title");
      this.updateTitle(title);
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (oldValue !== newValue) {
        if (name === "title") {
          this.updateTitle(newValue);
        }
      }
    }

    updateTitle(title) {
      this.shadowRoot.getElementById("title").innerHTML = title;
    }

    // updateBackEvent(back) {
    //     this.shadowRoot.getElementById('back').setAttribute('onclick', back);
    // }
  }

  customElements.define("app-header", Header);
})();
