(function sellCategoryFactory() {
  class SellCategory extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-sell-category");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-sell-category", SellCategory);
})();
(function sellLocationFactory() {
  class SellCategory extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-sell-location");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-sell-location", SellCategory);
})();
(function sellProductFactory() {
  class SellCategory extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-sell-product");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-sell-product", SellCategory);
})();
