(function buyCategoryFactory() {
  class BuyCategory extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-buy-category");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-buy-category", BuyCategory);
})();
(function buyLocationFactory() {
  class BuyCategory extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-buy-location");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-buy-location", BuyCategory);
})();
(function buyProductFactory() {
  class BuyCategory extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-buy-product");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-buy-product", BuyCategory);
})();
