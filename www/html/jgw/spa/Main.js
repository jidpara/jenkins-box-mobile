(function mainFactory() {
  class Main extends HTMLElement {
    constructor() {
      super();

      const template = document.getElementById("template-main");
      const templateContent = template.content;

      this.attachShadow({ mode: "open" }).appendChild(templateContent.cloneNode(true));
    }
  }

  customElements.define("page-main", Main);
})();
