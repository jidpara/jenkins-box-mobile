function goMain() {
  $(".page").hide();
  $("app-header").hide();
  $("page-main").removeClass("hidden").show();
}

function goBuyCategory() {
  $(".page").hide();
  $("app-header").attr("title", "구매하기").attr("back", "goMain()").removeClass("hidden").show();
  $("page-buy-category").removeClass("hidden").show();
}

function goBuyLocation() {
  $(".page").hide();
  $("app-header").attr("title", "지역선택").attr("back", "goBuyCategory()").removeClass("hidden").show();
  $("page-buy-location").removeClass("hidden").show();
}

function goBuyProduct() {
  $(".page").hide();
  $("app-header").attr("title", "상세정보").attr("back", "goBuyLocation()").removeClass("hidden").show();
  $("page-buy-product").removeClass("hidden").show();
}

function goSellCategory() {
  $(".page").hide();
  $("app-header").attr("title", "판매하기").attr("back", "goMain()").removeClass("hidden").show();
  $("page-sell-category").removeClass("hidden").show();
}

function goSellLocation() {
  $(".page").hide();
  $("app-header").attr("title", "지역선택").attr("back", "goSellCategory()").removeClass("hidden").show();
  $("page-sell-location").removeClass("hidden").show();
}

function goSellProduct() {
  $(".page").hide();
  $("app-header").attr("title", "상세정보").attr("back", "goSellLocation()").removeClass("hidden").show();
  $("page-sell-product").removeClass("hidden").show();
}
