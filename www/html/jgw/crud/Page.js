// get
function getProducts() {
  return api.get("products").then((productArray) => {
    if (Array.isArray(productArray)) {
      const ul = $("#list>ul");
      ul.html("");
      productArray.forEach((product) => {
        let nextPrice = parseInt(product.price);
        nextPrice = !isNaN(nextPrice) ? nextPrice : 0;
        nextPrice = nextPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        const li = $("<li>")
          .click(() => selectProduct(product.id))
          .append($("<span>").addClass("id").html(product.id))
          .append($("<span>").addClass("name").html(product.name))
          .append($("<span>").addClass("price").html(nextPrice))
          .append(
            $("<span>")
              .addClass("delete")
              .html("DEL")
              .click((e) => {
                deleteProduct(product.id);
                e.stopPropagation();
              })
          );
        ul.append(li);
      });
    }
  });
}
// post
function addProduct() {
  api
    .post("products", {
      name: $("[name=name]").val(),
      price: $("[name=price]").val(),
    })
    .then(getProducts)
    .then(clearForm);
}
// put
function updateProduct() {
  const id = $("[name=id]").val();
  api
    .put(`products/${id}`, {
      name: $("[name=name]").val(),
      price: $("[name=price]").val(),
    })
    .then(() => selectProduct(id))
    .then(getProducts)
    .then(goList);
}
// delete
function deleteProduct(id) {
  api.delete(`products/${id}`).then(getProducts);
}
// ui
function selectProduct(id) {
  return api.get(`products/${id}`).then((product) => {
    if (product !== undefined) {
      $("[name=id]").val(id);
      $("[name=name]").val(product.name);
      $("[name=price]").val(product.price);
      goEdit();
    }
  });
}

function onClickSetProduct() {
  const id = parseInt($("[name=id]").val());
  const name = $("[name=name]").val();
  if (name === "") {
    return;
  }
  if (isNaN(id)) {
    addProduct();
  } else {
    updateProduct(id);
  }
}

function clearForm() {
  $("[name=id]").val("");
  $("[name=name]").val("");
  $("[name=price]").val("");
}

// boot
(function () {
  getProducts();
})();
