function goList() {
  $("#title").html("Products List");
  $("#close").addClass("hidden");
  $("#list").removeClass("hidden");
  $("#form").addClass("hidden");
  $("#submit .text").html("추가");
  $("#submit").click(goAdd);
}

function goAdd() {
  $("#title").html("Product Add");
  $("#close").removeClass("hidden");
  $("#list").addClass("hidden");
  $("#form").removeClass("hidden");
  $("#submit .text").html("추가");
  $("#submit .text").click(addProduct);
}

function goEdit() {
  $("#title").html("Product Edit");
  $("#close").removeClass("hidden");
  $("#list").addClass("hidden");
  $("#form").removeClass("hidden");
  $("#submit .text").html("수정");
  $("#submit").click(updateProduct);
}

(function () {
  goList();
})();
