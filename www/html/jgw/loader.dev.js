(function loader() {
  function getParams() {
    let search = document.location.search;
    if (search.startsWith("?")) {
      search = search.substr(1);
    }
    return search.split("&").reduce((r, keyValue) => {
      if (!keyValue.includes("=")) {
        return r;
      }
      const [key, value] = keyValue.split("=");
      r[key.toLowerCase()] = value.toLowerCase();
      return r;
    }, {});
  }
  function openPage() {
    const params = getParams();
    let { page } = params;
    if (page === undefined) {
      page = "menu";
    }
    if (page !== undefined) {
      if (!page.endsWith(".html")) {
        page = `${page}.html`;
      }
      page = `./${page}`;
      fetch(page)
        .then((res) => res.text())
        .then((rowHtml) => {
          let tempContainer = document.createElement("div");
          tempContainer.innerHTML = rowHtml;
          document.getElementById("page").innerHTML = "";
          Array.from(tempContainer.children).forEach((child) => {
            if (child.tagName.toLowerCase() === "script") {
              const tempFunctionFactory = new Function(`return (function(){${child.innerHTML}})`);
              const tempFunction = tempFunctionFactory();
              tempFunction();
              return;
            }
            document.getElementById("page").appendChild(child);
          });
          delete tempContainer;
        });
    }
  }

  api.get(`mypage/profile/${window.member.seq}`).then((response) => {
    window.member = { ...response };
    openPage();
  });
})();
