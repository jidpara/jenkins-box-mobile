/**
 * CDN에서 관리하는 공통 js 파일
 * 남용 금지 - 심각한 이슈처리나 출시에 관계없이 처리해야 하는 내용만 사용 할 것
 * <script src="http://gk337-config-bucket.s3.ap-northeast-2.amazonaws.com/js/jgw.js"></script>
 */

var JgwUtil = {
  maintainNotice: function () {
    // const alertDialog = new AlertDialog("[0001]", "서비스점검중입니다. \n이용에불편을드려죄송합니다.  \n[서비스점검일시] \n2020.05.07 20:00 ~ 2020.05.07 23:59\n시스템사정에따라점검시간은변경될수있습니다.", "종료");
    try {
      const alertDialog = new AlertDialog(
        "[점검공지]",
        "서비스점검중입니다. \n이용에불편을드려죄송합니다.  \n\n[서비스점검일시] \n2020.05.07 20:00 ~ 2020.05.07 23:59\n시스템 사정에 따라 점검시간은 변경 될 수있습니다.",
        "종료",
        { closeBtn: false }
      );
      alertDialog.onClick = function (event) {
        if (event === "background") {
          return false;
        } else if (event === "ok") {
          cordova.plugins.exit();
        } else if (event === "close") {
          cordova.plugins.exit();
        } else {
          window.localStorage.setItem("lastUsedApp", "box337");
        }
      };
      alertDialog.show();
    } catch (e) {
      console.error(e);
    }
  },

  returnBox337: function () {
    if (window.gk337) {
      window.gk337.returnBox337();
    } else {
      window.location.href = window.location.href.split("www")[0] + "www/html/main.html";
    }
  },
};

// onload
(function () {
  window.JgwUtil = JgwUtil;
  // 점검공지
  setTimeout(function () {
    console.info("[jgw.js] nothing to do.");
    // 점검공지 표시
    //JgwUtil.maintainNotice();
  }, 3000);
})();
