let baseUrl = "http://webmobileapi-env.eba-d9trhysm.ap-northeast-2.elasticbeanstalk.com/api/";
if (window.ENV) {
  baseUrl = ENV.JGW_API_URL;
}
window.api.setBaseUrl(baseUrl);
window.api.onBefore(function before(request) {
  const { url } = request;
  if (url === "chat/messages/status" || url === "chat/messages/new") {
    return;
  }
  if (typeof window.showLoading === "function") {
    window.showLoading();
  }
});
window.api.onAfter(function after() {
  if (typeof window.hideLoading === "function") {
    window.hideLoading();
  }
});
window.api.setValidate(function validate(data) {
  if (data && data.error && data.error.code) {
    if (data.error.code !== "00") {
      return data.error;
    }
  }
});
