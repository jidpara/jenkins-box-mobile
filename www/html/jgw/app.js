// MISC 이 후에 정리
function getAllCategory() {
  return api.get("base/category", { parentSeq: "" }).then((allCategory) => {
    window.localStorage.setItem("allCategory", JSON.stringify(allCategory));
  });
}
function get1dpAllCategory() {
  return api.get("base/category/0", { parentSeq: "0" }).then((allCategory) => {
    window.localStorage.setItem("1dpCategory", JSON.stringify(allCategory));
  });
}
function formatNumber(num) {
  const nextNum = num === null || isNaN(num) ? 0 : num;
  return nextNum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
// fancyBox
function initFancyBox($) {
  function beforeShow(instance) {
    window.fancyBoxInstance = instance;
  }
  function afterClose() {
    window.fancyBoxInstance = undefined;
  }
  $("[data-fancybox]").fancybox({ beforeShow, afterClose, mobile: { clickSlide: "close" } });
}

getAllCategory();
get1dpAllCategory();

// 지마이다스 연동 상태
const launch = localStorage.getItem("launch");
if (launch && launch !== "") {
  window.ENV.launch = launch;
  localStorage.removeItem("launch");
}

// BOX337 회원정보 연동 임시 코드
let loginInfo = null;
let memberInfo = null;
let pushData = null;

if (window.gk337) {
  console.log("window.gk337.onBackButton listen.");
  window.gk337.onBackButton = () => {
    const back = getNavigateBack();
    if (back && typeof back === "function") {
      back();
    } else {
      window.gk337.confirmDialog(
        "앱을 종료하시겠습니까?",
        (index) => {
          if (index === 2) {
            navigate("finish");
            // window.gk337.finishApp();
          } else if (index === 1) {
            window.localStorage.removeItem("notiAppData");
            window.gk337.returnBox337();
          }
        },
        "확인",
        // ["종료하기", "아니오", "박스337 가기"],
        ["BOX337 가기", "종료하기", "아니오"]
      );
    }
  };

  returnBox337 = () => {
    if (window.gk337) {
      window.gk337.returnBox337();
    }
  };

  speechRec = () => {
    if (window.gk337) {
      return window.gk337.speechRecognition();
    }
  };
  window.gk337.getLoginInfo().then(bootUp).catch(bootUp);
} else {
  bootUp();
}
