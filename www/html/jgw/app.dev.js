window.navigate.setDev();
function getAllCategory() {
  return api.get("base/category", { parentSeq: "" }).then((allCategory) => {
    window.localStorage.setItem("allCategory", JSON.stringify(allCategory));
  });
}

function get1dpAllCategory() {
  return api.get("base/category/0", { parentSeq: "0" }).then((allCategory) => {
    window.localStorage.setItem("1dpCategory", JSON.stringify(allCategory));
  });
}

function formatNumber(num) {
  const nextNum = num === null || isNaN(num) ? 0 : num;
  return nextNum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
window.GET_PARAMETER = window.getParameter;
getAllCategory();
get1dpAllCategory();
kakaoToken = "27255bb4fdbcadd0cdc0f01d5f631d86";
RELOAD = () => {};
UNLOAD = () => {};
root = {};

// SWIPER UPDATE 공통 처리
setTimeout(function () {
  $(".swiper-wrapper").each(function (index, item) {
    // 대상 node 선택
    const target = $(this).get(0);

    let slideMedium;
    let slideSmall;
    let swiperWide;
    let slideNest;

    if ($(this).parent().hasClass("slide-small")) {
      slideSmall = new Swiper($(this).closest(".slide-small"), {
        slidesPerView: "auto",
        spaceBetween: 10,
        slidesOffsetBefore: 14,
        slidesOffsetAfter: 14,
      });
    }

    if ($(this).parent().hasClass("slide-medium")) {
      slideMedium = new Swiper($(this).closest(".slide-medium"), {
        slidesPerView: "auto",
        spaceBetween: 10,
        slidesOffsetBefore: 14,
        slidesOffsetAfter: 14,
      });
    }

    if ($(this).parent().hasClass("slide-wide")) {
      swiperWide = new Swiper($(this).closest(".slide-wide"), {
        pagination: {
          el: ".swiper-pagination",
        },
      });
    }

    if ($(this).parent().hasClass("slide-nest")) {
      slideNest = new Swiper($(this).closest(".slide-nest"), {
        lazy: {
          loadPrevNext: true,
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        allowTouchMove: false,
      });
    }

    // const slideSmall = new Swiper(`${$(this).get}.slide-small`, {
    //   slidesPerView: "auto",
    //   spaceBetween: 10,
    //   slidesOffsetBefore: 14,
    //   slidesOffsetAfter: 14,
    // });

    if (target instanceof Element || target instanceof HTMLDocument) {
      // CHILD NODE 변경 시 이벤트
      const observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
          console.log(mutation.type);
          if (mutation.type === "childList") {
            if (slideSmall) {
              slideSmall.update();
            }
            if (slideMedium) {
              slideSmall.update();
            }
            if (swiperWide) {
              swiperWide.update();
            }
          }
        });
      });

      // 감시자 옵션 포함, 대상 노드에 전달
      observer.observe(target, { childList: true });
    }
  });
}, 300);
