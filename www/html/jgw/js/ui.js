// TODO : 사용여부 확인
$(function () {
  function checkIOS() {
    let varUA = navigator.userAgent.toLowerCase();
    if (varUA.match("android") != null) {
      $("body").addClass("android");
    } else if (varUA.indexOf("iphone") > -1 || varUA.indexOf("ipad") > -1 || varUA.indexOf("ipod") > -1) {
      $("body").addClass("ios");
    }
  }

  checkIOS();

  $(".js-tab a").on("click", function (e) {
    e.preventDefault();
    $(this).closest(".js-tab").find("a").removeClass("on");
    $(this).addClass("on");
    let index = $(this).closest(".js-tab").find("a").index(this);
    $(this).closest(".js-tab").next().find(".js-tab-item").removeClass("on");
    $(this).closest(".js-tab").next().find(".js-tab-item").eq(index).addClass("on");
  });

  let btnAccordion = $(".js-accordion .btn-accordion");

  btnAccordion.eq(0).trigger("click");
  btnAccordion.on("click", function () {
    $(this).toggleClass("on");
    btnAccordion.not(this).removeClass("on");
    return false;
  });
  btnAccordion.eq(0).trigger("click");

  $(".toggle").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("on");
  });
  $("textarea.js-auto").on("keydown keyup", function () {
    $(this)
      .height(1)
      .height($(this).prop("scrollHeight") + 12);
  });
  let chatArea = $("textarea.js-auto-chat");
  chatArea
    .on("focus", function () {
      autosize($(".js-auto-chat"));
    })
    .on("blur", function () {
      autosize.destroy($(this));
    });
  $(".btn-sort").on("click", function (e) {
    e.preventDefault();
    $(".sheet-wrap, .bg-dim").addClass("open");
  });

  $(".bg-dim").on("click", function () {
    $(".wrap, .header-top").removeAttr("style");
    $(".sheet-wrap, .bg-dim, .pop-wrap").removeClass("open");
  });
  $(".btn-sheet").on("click", function (e) {
    e.preventDefault();
    if (!$(this).hasClass("cancel")) {
      $(".btn-sheet").removeClass("active");
      $(this).addClass("active");
      $(".btn-sort span").text($(this).find("span").text());
    }

    $(".bg-dim").trigger("click");
  });
  $(".pop-head .btn-close-large, .btn-pop-confirm").on("click", function (e) {
    e.preventDefault();
    $(".pop-wrap").removeClass("open");
    $(".bg-dim").trigger("click");
  });
  let categoryCheck = $(".category-check input[type=checkbox]");
  let mapCheck = $(".map-check");
  let isCategoryCheck = (obj) => {
    if (obj.is(":checked")) {
      obj.closest("label").addClass("checked");
    } else {
      obj.closest("label").removeClass("checked");
    }
  };
  let isLocalCheck = (obj) => {
    obj.toggleClass("checked");
  };
  categoryCheck.each(function (idx, item) {
    isCategoryCheck($(this));
  });
  categoryCheck.on("click", function () {
    isCategoryCheck($(this));
  });

  mapCheck.on("click", function () {
    isLocalCheck($(this));
  });
});
//get img width height
jQuery.event.add(window, "load", function () {
  $(".slide-wide img, .s-thumb-l img").each(function (idx, item) {
    if ($(this)[0].naturalHeight < $(this)[0].naturalWidth) {
      $(this).addClass("landscape");
    }
  });
});

(function () {
  let slideMedium = new Swiper(".slide-medium", {
    slidesPerView: "auto",
    spaceBetween: 10,
    slidesOffsetBefore: 14,
    slidesOffsetAfter: 14,
  });
  let slideSmall = new Swiper(".slide-small", {
    slidesPerView: "auto",
    spaceBetween: 10,
    slidesOffsetBefore: 14,
    slidesOffsetAfter: 14,
  });
  let swiperWide = new Swiper(".slide-wide", {
    pagination: {
      el: ".swiper-pagination",
    },
  });
  let swiperSplash = new Swiper(".slide-splash", {
    pagination: {
      el: ".swiper-pagination",
    },
    navigation: {
      nextEl: ".js-next",
    },
  });
})();
