// 클라이언트 모듈
window.gk337 = {
  jgwMember: {},
  deviceInfo: {},
  ipc_: null,
  onOnline: function () {},
  onOffline: function () {},
  onGeolocation: function (position) {},
  onBackButton: function () {},
  /** @description SimInfo 응답. 사용자 정의. window.gk337.onSimInfo= result => {//doSomething}
   */
  onSimInfo: function (result) {},
  initialize: function () {
    const self = this;
    document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
    document.addEventListener(
      "offline",
      function () {
        self.onOffline && self.onOffline();
      },
      false
    );
    document.addEventListener(
      "online",
      function () {
        self.onOnline && self.onOnline();
      },
      false
    );

    document.addEventListener(
      "backbutton",
      function () {
        self.onBackButton && self.onBackButton();
      },
      false
    );
  },
  onDeviceReady: function () {
    this.deviceInfo = device;
    const self = this;
    FirebasePlugin.getToken(
      function (fcmToken) {
        self.deviceInfo.notiToken = fcmToken;
      },
      function (error) {
        console.error(error);
      }
    );
  },
  /**
   * @description device정보 가져오는 비동기 함수
   */
  getDeviceInfo: function () {
    const self = this;
    return new Promise(function (resolve, reject) {
      const interval = 100;
      let elapsed = 0;
      const awaitDeviceInfo = setInterval(function () {
        if (elapsed > 1000) {
          clearInterval(awaitDeviceInfo);
          reject("getDeviceInfo Timeout");
        }
        elapsed += interval;

        if (self.deviceInfo && self.deviceInfo.notiToken) {
          clearInterval(awaitDeviceInfo);
          resolve(self.deviceInfo);
        }
      }, interval);
    });
  },
  // init: function(context) {
  //   console.log("context", context);
  //   global = context;
  // },
  /** @description alert dialog 메시지를 띄웁니다.
   * @param {string} message 메시지
   * @param {string} callbackName alert 메시지가 닫히고 난 후 실행할 콜백 함수 명
   * @param {string} title 제목
   * @param {string} buttonName 버튼명 기본값. OK
   */
  alertDialog: function (message, callback, title, buttonName) {
    navigator.notification.alert(message, callback, title, buttonName);
  },
  /** @description confirm dialog 메시지를 띄웁니다.
   * @param {string} message 메시지
   * @param {string} callbackName confirmDialog가 닫히고 난 후 실행할 콜백 함수 명입니다. index는 1부터 시작이고, 0은 배경을 눌러서 사라졌을 때 입니다.
   * @param {string} title 제목
   * @param {Array<string>} buttonLabels 버튼 목록 기본값.  [OK,Cancel]
   */
  confirmDialog: function (message, callback, title, buttonLabels) {
    navigator.notification.confirm(message, callback, title, buttonLabels);
  },
  /** @description toast 메시지를 띄웁니다.
   * @param {string} message 메시지
   * @param {string || number} duration 'short', 'long', '3000', 900 (the latter are milliseconds)
   * @param {string} position 'top', 'center', 'bottom'
   */
  toast: function (message, duration, position) {
    plugins.toast.showWithOptions({
      message: message,
      duration: duration,
      position: position,
    });
  },
  /** @description Geolocation 정보를 요청합니다. onGeolocation으로 응답이 옵니다.
   */
  requestGeolocation: function () {
    const self = this;

    const onSuccess = (position) => {
      console.log("position", position);
      var positionObject = {};

      if ("coords" in position) {
        positionObject.coords = {};

        if ("latitude" in position.coords) {
          positionObject.coords.latitude = position.coords.latitude;
        }
        if ("longitude" in position.coords) {
          positionObject.coords.longitude = position.coords.longitude;
        }
        if ("accuracy" in position.coords) {
          positionObject.coords.accuracy = position.coords.accuracy;
        }
        if ("altitude" in position.coords) {
          positionObject.coords.altitude = position.coords.altitude;
        }
        if ("altitudeAccuracy" in position.coords) {
          positionObject.coords.altitudeAccuracy = position.coords.altitudeAccuracy;
        }
        if ("heading" in position.coords) {
          positionObject.coords.heading = position.coords.heading;
        }
        if ("speed" in position.coords) {
          positionObject.coords.speed = position.coords.speed;
        }
      }

      if ("timestamp" in position) {
        positionObject.timestamp = position.timestamp;
      }
      self.onGeolocation && self.onGeolocation(positionObject);
    };

    function onError(error) {
      self.onGeolocation && self.onGeolocation(error);
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
  },
  /** @description Sim 정보를 요청합니다. onSimInfo으로 응답이 옵니다.
   */
  requestSimInfo: function () {
    const self = this;
    const { platform } = device;
    if (platform === "Android") {
      window.plugins.sim.hasReadPermission(
        function (info) {
          console.log("Has permission:", info);
          if (info === false) {
            window.plugins.sim.requestReadPermission(
              function () {
                console.log("granted");
                console.log("requestReadPermission this", self);
                self.responeSimInfo();
              },
              function () {
                console.log("denied");
                self.responeSimInfo();
              }
            );
          } else {
            self.responeSimInfo();
          }
        },
        function (error) {
          self.onSimInfo(error);
        }
      );
    } else if (platform === "iOS") {
      self.responeSimInfo();
    }
  },
  /**
   * @description 심 정보 응답
   */
  responeSimInfo: function () {
    console.log("responeSimInfo this", this);
    const self = this;
    function onSuccess(result) {
      console.log("onSuccess this", self);
      self.onSimInfo && self.onSimInfo(result);
    }
    function onError(error) {
      self.onSimInfo && self.onSimInfo(error);
    }
    window.plugins.sim.getSimInfo(onSuccess, onError);
  },
  /**
   * @description 로그인 정보를 가져옵니다.
   */
  getLoginInfo: function () {
    const user = localStorage.getItem("loginid");
    console.log("user", user);
    return new Promise(function (resolve, reject) {
      if (user === undefined) throw "there is no loginInfo";
      fetch(`https://admin.gk337.co.kr/app/account/${user}`, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        headers: { "*": "*" },
        //body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (responseJson) {
          const { account } = responseJson;
          if (account) {
            const { zipcode, addr1, addr2, image, nameko, nameus } = account;
            localStorage.setItem("zipCode", zipcode);
            localStorage.setItem("addr1", addr1);
            localStorage.setItem("addr2", addr2);
            localStorage.setItem("image", image);
            localStorage.setItem("nameko", nameko);
            localStorage.setItem("nameus", nameus);

            resolve(window.localStorage);
          } else {
            throw "failed get account info";
          }
        })
        .catch(function (err) {
          reject(err);
        });
    });
  },
  /** @description 중고왕 연결 함수
   *     @param {notiAppData} 노티로 연결시 넘겨주는 데이터
   */
  connectJGW: function (notiAppData) {
    if (!ENV) {
      return;
    }

    const { loginid } = window.localStorage;
    if (!loginid) {
      window.localStorage.setItem("loginRedirectApp", "jgw");
      window.location.href = window.location.href.split("www")[0] + "www/html/login.html";
    }

    const self = this;
    Promise.all([this.getLoginInfo(), this.getDeviceInfo()]).then(function (values) {
      const loginInfo = values[0];
      const deviceInfo = values[1];

      const { loginid, nameko, nameus, zipCode, addr1, addr2, image } = loginInfo;

      deviceInfo.deviceToken = self.deviceInfo.notiToken;
      const param = {
        deviceInfo: deviceInfo,
        userId: loginid,
        memName: nameko || nameus,
        memZip: zipCode,
        memAddr: addr1,
        memAddrDetail: addr2,
        profileImage: image,
      };

      //notiAppData가 있을 경우 로컬스토리지 저장
      if (notiAppData) {
        window.localStorage.setItem("notiAppData", notiAppData);
      }

      // ENV 인식 못했을 임시 방어 코드.... :(
      let baseUrl = "http://webmobileapi-env.eba-d9trhysm.ap-northeast-2.elasticbeanstalk.com/api/";
      if (window.ENV) {
        baseUrl = ENV.JGW_API_URL;
      }
      $.ajax({
        url: `${baseUrl}base/connect`,
        type: "post",
        data: JSON.stringify(param),
        crossDomain: true,
        contentType: "application/json",
        success: function (res) {
          const { error } = res;
          if (error && res.error.code === "00") {
            window.member = res.memberInfo;
            window.member.memSeq = res.memberInfo.seq;
            window.localStorage.setItem("member", JSON.stringify(window.member));
            // 중고왕 메인 이동 처리
            if (window.location) {
              window.localStorage.setItem("lastUsedApp", "jgw");
              // 호출 위치에 따라 변경 상대경로 사용 불가
              window.location.href = window.location.href.split("www")[0] + "www/html/jgw/index.html";
            }
          } else if (error && res.error.code !== "00") {
            alert(error.message);
          }
        },
        error: function (xhr, status, error) {
          alert("중고왕 연결 실패");
          console.error("error : ", error);
        },
      });
    });
  },
  /**
   * @description 박스337로 돌아가는 기능
   */
  returnBox337: function () {
    // 중고왕 && KNOX 앱은 box337 앱을 실행함
    if (window.ENV.KNOX_YN === "Y") {
      // Native 연동 코드
      // // Default handlers
      // const successCallback = function (data) {
      //   console.log("data : ", data);
      //   window.plugins.launcher.launch(
      //     { packageName: "com.gibubox" },
      //     (data) => {
      //       console.info("launched box337 successfully", data);
      //     },
      //     errorCallback
      //   );
      // };
      // const errorCallback = function (errMsg) {
      //   let URL = "market://details?id=com.gibubox";
      //   const target = "_system";
      //   if (URL !== "") {
      //     window.open(URL, target);
      //   }
      // };
      // window.plugins.launcher.canLaunch({ packageName: "com.gibubox" }, successCallback, errorCallback);
      alert("준비중입니다.");
    } else {
      window.localStorage.setItem("lastUsedApp", "box337");
      window.location.href = "../main.html";
    }
  },
  // /**
  //  * @description 사진찍기, 갤러리 선택 팝업 띄우기. 선택된 이미지 리턴함
  //  */
  // showImagePicker: function () {
  //   const self = this;
  //   return new Promise(function (resolve) {
  //     if (self.ipc_ === null || self.ipc_ === undefined) {
  //       self.ipc_ = new ImagePickerCamera();
  //     }

  //     self.ipc_.onSelect = function (imageUrl) {
  //       resolve(imageUrl);
  //     };
  //     self.ipc_.showPopup();
  //   });
  // },
  /**
   * @description 앱 즉시 종료
   */
  finishApp: function () {
    cordova.plugins.exit();
  },
  /**
   * @description 음성인식 팝업 호출 및 결과 리턴
   * @return Promise
   */
  speechRecognition: function () {
    return new Promise(function (resolve, reject) {
      window.speechRecognition
        .isRecognitionAvailable()
        .then(function (available) {
          if (available) {
            return window.speechRecognition.hasPermission();
          }
        })
        .then(function (hasPermission) {
          function startRecognition() {
            return window.speechRecognition
              .startRecognition({
                language: "ko-KR",
                matches: 1,
                showPopup: true,
                showPartial: true,
              })
              .then(function (data) {
                console.log("Results", data);
                resolve(data);
              })
              .catch(function (err) {
                console.error(err);
                reject(err);
              });
          }

          if (!hasPermission) {
            window.speechRecognition
              .requestPermission()
              .then(function () {
                startRecognition();
              })
              .catch(function (err) {
                console.error("Cannot get permission", err);
                reject(err);
              });
          } else {
            startRecognition();
          }
        })
        .catch(function (err) {
          console.error(err);
          reject(err);
        });
    });
  },
};

window.gk337.initialize();
