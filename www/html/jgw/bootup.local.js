function bootUp(response) {
  loginInfo = response;
  try {
    memberInfo = JSON.parse(loginInfo.member);
    pushData = JSON.parse(window.localStorage.getItem("notiAppData"));
  } catch (e) {
    // local web case
  }

  if (memberInfo && memberInfo.memSeq) {
    window.member = { ...memberInfo };
  }
  // Test Code
  // push data로 deep link 처리
  // push url 있는 경우 splash 스킵 처리
  document.addEventListener(
    "deviceready",
    () => {
      if (pushData && pushData.url) {
        navigate("root");
        navigate(pushData.url, { ...pushData.param });
        setTimeout(() => {
          window.localStorage.removeItem("notiAppData");
        }, 1000);
      } else {
        navigate("splash");
      }
    },
    false
  );

  // Local 실행을 위하여, deviceready 이벤트 강제 발생
  document.dispatchEvent(new Event("deviceready"));
}
