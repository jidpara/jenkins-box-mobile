(function () {
  function checkIOS() {
    let varUA = navigator.userAgent.toLowerCase();
    if (varUA.match("android") != null) {
      //안드로이드 일때 처리
      $("body").addClass("android");
    } else if (varUA.indexOf("iphone") > -1 || varUA.indexOf("ipad") > -1 || varUA.indexOf("ipod") > -1) {
      $("body").addClass("ios");
    }
  }
  checkIOS();
})();
