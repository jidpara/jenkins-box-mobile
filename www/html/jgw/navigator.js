(function navigatorFactory() {
  function getLastPage() {
    const pages = Array.from(document.getElementsByTagName("app-page"));
    return pages[pages.length - 1];
  }

  function navigate(page, parameter) {
    if (dev === true) {
      return navigateDev(page, parameter);
    }
    // const lastPage = getLastPage();
    // if (lastPage !== undefined && lastPage !== null) {
    //   lastPage.tryRelease();
    // }
    const pageEl = document.createElement("app-page");
    pageEl.setAttribute("page", page);
    pageEl.openPageAsync(parameter);
  }

  function navigateToBack(_page, parameter) {
    if (dev === true) {
      return navigateDev(_page, parameter);
    }

    if (window.fancyBoxInstance !== undefined) {
      window.fancyBoxInstance.close();
      window.fancyBoxInstance = undefined;
      return;
    }

    const currentPage = getLastPage();
    let pages = Array.from(document.getElementsByTagName("app-page"));
    if (_page !== undefined) {
      let isFound = false;
      pages.forEach((c) => {
        if (typeof c.getAttribute === "function") {
          const page = c.getAttribute("page");
          if (page === _page) {
            c.reload(parameter);
            isFound = true;
            return;
          }
        }
        if (c === currentPage) {
          return;
        }
        if (isFound && typeof c.releasePage === "function") {
          c.releasePage();
        }
      });
    } else {
      const backwardPage = pages[pages.length - 2];
      if (backwardPage !== undefined && backwardPage !== null) {
        backwardPage.reload(parameter);
      }
    }

    if (currentPage !== undefined && currentPage !== null) {
      currentPage.closePage().then(() => {
        pages = Array.from(document.getElementsByTagName("app-page"));
        if (pages.length === 0) {
          navigateToRoot();
        }
      });
    } else {
      pages = Array.from(document.getElementsByTagName("app-page"));
      if (pages.length === 0) {
        navigateToRoot();
      }
    }
  }

  function navigateAndReplace(_page, parameter) {
    const currentPage = getLastPage();
    navigate(_page, parameter);
    if (typeof currentPage.releasePage === "function") {
      currentPage.releasePage();
    }

    if (currentPage !== undefined && currentPage !== null) {
      currentPage.closePage();
    }
  }

  function navigateToRoot(_page, parameter) {
    if (dev === true) {
      return navigateDev();
    }
    if (_page === undefined || _page === null) {
      const currentPage = getLastPage();
      const pages = Array.from(document.getElementsByTagName("app-page"));
      pages.forEach((c) => {
        if (c === "root") {
          return;
        }
        if (typeof c.releasePage === "function") {
          c.releasePage();
        }
      });
      if (Array.from(document.getElementsByTagName("app-page")).length === 0) {
        navigate("root", parameter);
      }
    } else {
      const currentPage = getLastPage();
      const pages = Array.from(document.getElementsByTagName("app-page"));
      pages.forEach((c) => {
        if (c === currentPage) {
          return;
        }
        if (c === "root") {
          return;
        }
        if (typeof c.releasePage === "function") {
          c.releasePage();
        }
      });
    }
  }
  function getNavigateBack(page) {
    if (dev === true) {
      if (page !== undefined) {
        return () => navigate(page);
      }
      if (document.location.search.indexOf("page=") > -1) {
        return navigateToRoot;
      }
      return;
    }
    const lastPage = getLastPage();
    if (lastPage !== undefined && lastPage !== null) {
      const page = lastPage.getAttribute("page");
      if (page !== "root") {
        return navigateToBack;
      }
    }
  }
  function getParameter(parameter) {
    if (dev === true) {
      return getParameterDev();
    }
    return parameter;
  }
  function boot() {
    window.navigate = navigate;
    window.navigateToBack = navigateToBack;
    window.navigateToRoot = navigateToRoot;
    window.navigateAndReplace = navigateAndReplace;
    window.getNavigateBack = getNavigateBack;
    window.getParameter = getParameter;
    window.navigate.setDev = setDev;
  }

  // Dev Mode
  let dev = false;
  function setDev() {
    dev = true;
  }
  function navigateDev(page, parameter) {
    let href = "dev.html";
    if (page !== undefined) {
      href = `${href}?page=${page}`;
      if (parameter !== null && typeof parameter === "object") {
        Object.keys(parameter).forEach((key) => {
          href = `${href}&${key}=${parameter[key]}`;
        });
      }
    }
    document.location.href = href;
  }
  function getParameterDev() {
    let search = document.location.search;
    if (search.startsWith("?")) {
      search = search.substr(1);
    }
    return search.split("&").reduce((r, keyValue) => {
      if (!keyValue.includes("=")) {
        return r;
      }
      const [key, value] = keyValue.split("=");
      if (key.toLowerCase() !== "page") {
        r[key] = value;
      }
      return r;
    }, {});
  }
  // Boot
  boot();
})();
/**
 * History 는 없다.
 * navigate 하면 page 가 마지막 요소로 추가된다.
 * back 하면 현재 page 가 제거된다.
 * root 하면 root 를 제외한 모든 페이지가 제거된다.
 * back / root 는 돌아가는 페이지를 reload 한다.
 * back / root 하였는데, container 에 아무것도 없으면, root 를 추가한다.
 */
