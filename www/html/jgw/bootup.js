function bootUp(response) {
  loginInfo = response;
  try {
    memberInfo = JSON.parse(loginInfo.member);
    pushData = JSON.parse(window.localStorage.getItem("notiAppData"));
    if (pushData && pushData.url) {
      // navigate("root");
      api.get(`mypage/profile/${memberInfo.seq}`).then((response) => {
        window.member = { ...response };
        navigate(pushData.url, { ...pushData.param });
      });
    } else {
      navigate("splash");
    }
    window.localStorage.removeItem("notiAppData");
  } catch (e) {
    // local web case
  }

  if (memberInfo && memberInfo.memSeq) {
    window.member = { ...memberInfo };
  }
}
