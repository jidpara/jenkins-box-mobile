var _mode = "DEV";
var _version = "2.18.0";
var _platform = "android";
var _knox_yn = "N";

// WEB & Local CASE
_mode = _mode.indexOf("__") !== -1 ? "DEV" : _mode;
_version = _version.indexOf("__") !== -1 ? "2.5.7" : _version;
_platform = _platform.indexOf("__") !== -1 ? "WEB" : _platform;
_knox_yn = _knox_yn.indexOf("__") !== -1 ? "N" : _knox_yn;

const defaultEnv = {
  //예외 사업자 번호
  TEST_BIZ_REG_NUM: "1234567890",
};
const ENV_MAP = {
  LOCAL: {
    ...defaultEnv,
    MODE: "DEV",
    BOX337_API_URL: "http://localhost:8080/app",
    JGW_API_URL: "http://localhost:18080/api/",
    // API_URL: 'http://10.0.2.2:18080',
    VERSION: _version,
    PLATFORM: _platform,
    KNOX_YN: _knox_yn,
  },
  DEV: {
    ...defaultEnv,
    MODE: "DEV",
    BOX337_API_URL: "https://admin.gk337.co.kr/app",
    JGW_API_URL: "http://ec2-13-209-96-138.ap-northeast-2.compute.amazonaws.com:18080/api/",
    // API_URL: 'http://10.0.2.2:8080',
    VERSION: _version,
    PLATFORM: _platform,
    KNOX_YN: _knox_yn,
  },
  PRD: {
    ...defaultEnv,
    MODE: "PRD",
    BOX337_API_URL: "https://admin.gk337.co.kr/app",
    JGW_API_URL: "https://jgw-api.gk337.co.kr/api/",
    VERSION: _version,
    PLATFORM: _platform,
    KNOX_YN: _knox_yn,
  },
};

var ENV = ENV_MAP[_mode];
