var index = {
  // Application Constructor
  initialize: function () {
    document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
  },

  // deviceready Event Handler
  onDeviceReady: function () {
    this.checkUpdate().then((result) => {
      if (result && result.hasUpdate) {
        document.body.style.display = "";
        this.receivedLaterEvent("later", result.isMandatory);
        this.receivedUpdateEvent("install", result.downloadURL);
        this.receivedStoreEvent("store");
      } else {
        this.startApp();
      }
    });

    // 지마이다스 앱 연동 유니버셜 링크
    // Link Event
    // link/launch
    universalLinks.subscribe(null, function (eventData) {
      console.log("eventData", JSON.stringify(eventData));
      const hash = eventData.params.hash;
      const id = eventData.params.id;
      localStorage.setItem("hash", hash);
      localStorage.setItem("loginid", id);
      localStorage.setItem("login", true);
      localStorage.setItem("loginRedirectApp", "jgw");
      localStorage.setItem("launch", "gmidas");
      console.log("launch", localStorage.getItem("launch"));
      window.gk337.connectJGW();
    });
  },

  startApp: function () {
    localStorage.removeItem("launch");
    if(ENV.KNOX_YN === "Y"){
      window.gk337.connectJGW();
    }else{
      location.replace("main.html");
    }
  },

  receivedStoreEvent: function (id) {
    const element = document.getElementById(id);

    element.addEventListener("click", function () {
      if (ENV.PLATFORM === "android") {
        URL = "market://details?id=com.gibubox";
      } else if (ENV.PLATFORM === "ios") {
        URL = "https://apps.apple.com/us/app/box337/id1499196040?l=ko&ls=1";
      }
      var target = "_system";
      if (URL !== "") {
        window.open(URL, target);
      }
    });
  },
  receivedLaterEvent: function (id, isMandatory) {
    const element = document.getElementById(id);
    if (isMandatory) {
      element.style.display = "none";
    } else {
      element.addEventListener("click", function () {
        this.startApp();
      });
    }
  },
  receivedUpdateEvent: function (id, downloadURL) {
    const element = document.getElementById(id);

    element.addEventListener(
      "click",
      function () {
        const fileTransfer = new FileTransfer();

        // Get cordova file data directory (app sandbox directory)
        //  > file:///data/user/0/io.cordova.apk.installer.sample/files/
        let sandBoxDirectory = cordova.file.dataDirectory;

        // Please set apk download path
        let apkUrl = downloadURL;

        // Get file name by apk url;
        let fileName = apkUrl.match(/[^/]+$/i)[0];

        let modal = document.querySelector("ons-modal");

        modal.show();

        // 다운로드 프로그래스바 init
        cordova.plugin.progressDialog.init({
          progressStyle: "HORIZONTAL",
          title: "잠시만 기다려주세요...",
          message: "업데이트 중...",
        });
        // APK 다운로드 시작
        fileTransfer.download(
          apkUrl,
          sandBoxDirectory + fileName,
          function (entry) {
            console.log("download success: " + entry.toURL());
            // hide progress
            cordova.plugin.progressDialog.dismiss();
            // Install app
            apkInstaller.install(
              fileName,
              function (msg) {
                // Start the installer
                modal.hide();
              },
              function (error) {
                modal.hide();
                ons.notification.alert("Install Error !!: " + error);
              }
            );
          },
          function (error) {
            modal.hide();
            ons.notification.alert("Download Error !!: " + error);
          },
          false,
          {}
        );

        // 다운로드 프로그래스바 설정
        let count = 0;
        fileTransfer.onprogress = function (progressEvent) {
          console.log("onprogress >> ", progressEvent);
          if (progressEvent.lengthComputable) {
            console.warn(progressEvent.loaded / progressEvent.total);
            var percentage = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            cordova.plugin.progressDialog.setProgress(percentage);
          } else {
            cordova.plugin.progressDialog.setProgress(count++);
          }
        };
      },
      false
    );
  },

  checkUpdate: function () {
    return new Promise(function (resolve, reject) {
      let result = {
        hasUpdate: false,
        isMandatory: false,
        downloadURL:
          "https://jungoking-bucket-assets.s3.ap-northeast-2.amazonaws.com/upload/APK/knox/app-release-box337-PRD.apk",
      };
      // 앱 업데이트 버전 체크
      console.info("[INDEX] CURRENT APP VERSION : ", window.ENV.VERSION);
      if (window.ENV) {
        baseUrl = ENV.JGW_API_URL;
      }
      $.ajax({
        url: `${baseUrl}base/policy/UPDATE`,
        type: "get",
        cache: false,
        crossDomain: true,
        contentType: "application/json;charset=UTF-8",
        success: function (res) {
          const { baseUpdatePolicy } = res;
          if (baseUpdatePolicy) {
            const appVersion = Number(ENV.VERSION.replace(/\D/gi, ""));
            const minVersion = Number(baseUpdatePolicy.minVersion.replace(/\D/gi, ""));

            console.info("[INDEX] appVersion : ", appVersion);
            console.info("[INDEX] minVersion : ", minVersion);

            if (appVersion < minVersion) {
              result.hasUpdate = true;

              // 별도 다운로드URL 내려올 경우
              // TODO : BOX337은 우선 고정값 사용
              // if (baseUpdatePolicy.downloadURL !== null && baseUpdatePolicy.downloadURL.length > 0) {
              //   result.downloadURL = baseUpdatePolicy.downloadURL;
              // }
              // 필수 업데이트 확인
              if (baseUpdatePolicy.updateType === "mandatory") {
                result.isMandatory = true;
              }
            } else {
              result.hasUpdate = false;
              resolve(result);
            }

            resolve(result);
          } else {
            resolve(null);
          }
        },
        error: function (xhr, status, error) {
          alert("업데이트 정책 다운로드 실패");
          console.error("error : ", error);
          resolve(null);
        },
      });
    });
  },
};

index.initialize();
