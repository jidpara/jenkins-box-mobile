/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var common = {
  isActive: true,
  // Application Constructor
  initialize: function () {
    const self = this;
    console.log("initialize");

    document.addEventListener("deviceready", this.onDeviceReady.bind(this), { once: true });
    document.addEventListener("resume", this.onDeviceResume.bind(this), false);
    document.addEventListener("pause", this.onDevicePause.bind(this), false);
    document.addEventListener(
      "offline",
      function () {
        self.onOffline && self.onOffline();
      },
      false
    );
    document.addEventListener(
      "online",
      function () {
        self.onOnline && self.onOnline();
      },
      false
    );
    // speechRecognition 공통
    // TODO: speechRecognition 관련 js파일 분리
    window["speechRecognition"] = {
      hasPermission: function () {
        return new Promise(function (resolve, reject) {
          window.plugins.speechRecognition.hasPermission(
            function (isGranted) {
              resolve(isGranted);
            },
            function (err) {
              reject(err);
            }
          );
        });
      },
      requestPermission: function () {
        return new Promise(function (resolve, reject) {
          window.plugins.speechRecognition.requestPermission(
            function () {
              resolve();
            },
            function (err) {
              reject();
            }
          );
        });
      },
      startRecognition: function (settings) {
        return new Promise(function (resolve, reject) {
          window.plugins.speechRecognition.startListening(
            function (result) {
              resolve(result);
            },
            function (err) {
              reject(err);
            },
            settings
          );
        });
      },
      getSupportedLanguages: function () {
        return new Promise(function (resolve, reject) {
          window.plugins.speechRecognition.getSupportedLanguages(
            function (result) {
              resolve(result);
            },
            function (err) {
              reject(err);
            }
          );
        });
      },
      isRecognitionAvailable: function () {
        return new Promise(function (resolve, reject) {
          window.plugins.speechRecognition.isRecognitionAvailable(
            function (available) {
              resolve(available);
            },
            function (err) {
              reject(err);
            }
          );
        });
      },
      stopListening: function () {
        return new Promise(function (resolve, reject) {
          window.plugins.speechRecognition.stopListening(
            function () {
              resolve();
            },
            function (err) {
              reject(err);
            }
          );
        });
      },
    };
  },
  // loadingComponent: null,
  // createLoading: function() {
  //   const spinAnimation = document.createElement("style");
  //   spinAnimation.textContent = ` @keyframes spin {
  //   0% {
  //     transform: rotate(0deg);
  //   }
  //   100% {
  //     transform: rotate(360deg);
  //   }
  // }`;
  //   document.head.appendChild(spinAnimation);

  //   this.loadingComponent = document.createElement("div");
  //   this.loadingComponent.style.cssText =
  //     "position:absolute;z-index:99999;background-color:white;width:100%;height:100%;display:none";
  //   const loadingCircle = document.createElement("div");
  //   loadingCircle.style.cssText = ` position: absolute;
  //     left: 50%;
  //     top: 50%;
  //     z-index: 1;
  //     margin: -75px 0 0 -75px;

  //     border: 16px solid #f3f3f3; /* Light grey */
  //     border-top: 16px solid #3498db; /* Blue */
  //     border-radius: 50%;
  //     width: 120px;
  //     height: 120px;
  //     animation: spin 2s linear infinite;`;
  //   this.loadingComponent.appendChild(loadingCircle);
  //   document
  //     .querySelector("body")
  //     .insertBefore(
  //       this.loadingComponent,
  //       document.querySelector("body").firstChild
  //     );
  // },
  onOnline: function () {
    const networkState = navigator.connection.type;
    if (networkState === Connection.NONE) {
      this.onOffline();
    }
  },
  onOffline: function () {
    const alertDialog = new AlertDialog(
      "[네트워크 상태 알림]",
      "네트워크가 불안정 합니다.\n인터넷 연결 상태를 확인해주세요.",
      "재시도",
      { closeBtn: false }
    );
    alertDialog.onClick = function (event) {
      if (event === "background") {
        return false;
      } else if (event === "ok") {
        tryAgain();
      }
    };

    function tryAgain() {
      const networkState = navigator.connection.type;
      if (networkState === Connection.NONE) {
        setTimeout(() => alertDialog.show(), 50);
      }
    }

    tryAgain();
  },
  showLoadingScreen: function () {
    const loading = document.getElementById("loading");
    if (loading) {
      loading.style.display = "block";
    }
  },
  hideLoadingScreen: function () {
    const loading = document.getElementById("loading");
    if (loading) {
      loading.style.display = "none";
    }
  },

  onDeviceReady: function () {
    const self = this;
    setTimeout(function () {
      //초기 firebase messageReceived 처리후 active설정하도록
      self.isActive = true;
    }, 600);

    self.showLoadingScreen();

    // 매 페이지 이동시 노티로 진입시 이벤트 처리
    document.addEventListener("backbutton", onBackKeyDown, false);
    console.log("backbutton Event Listen");
    // Handle the back button
    //
    // TODO 다국어 확인
    function onBackKeyDown() {
      const { pathname } = window.location;
      if (pathname.indexOf("/jgw/index.html") > -1) {
        // 중고왕에서 이벤트 처리
        if (window.gk337 && window.gk337.onBackKeyDown) {
          window.gk337.onBackButton();
        }
      } else if (pathname.indexOf("/main.html") > -1) {
        navigator.notification.confirm("앱을 종료하시겠습니까?", onConfirm, "", ["취소", "종료"]);
      } else {
        history.back(1);
      }
    }

    function onConfirm(buttonIndex) {
      if (buttonIndex === 2) {
        //종료
        //app terminate
        cordova.plugins.exit();
      }
    }

    // 기본 채널 설정. 안드로이드 O 이후 부터는 채널로 설정됨.
    const channel = {
      id: "box337",
      name: "Box337",
      description: "",
      sound: "default",
      vibration: [2000, 200, 2000],
      light: true,
      lightColor: parseInt("FF0000FF", 16).toString(),
      importance: 4,
      badge: true,
      visibility: -1,
    };

    FirebasePlugin.setDefaultChannel(
      channel,
      function () {
        console.log("Default channel set");
      },
      function (error) {
        console.log("Set default channel error: " + error);
      }
    );

    FirebasePlugin.onMessageReceived(
      function (message) {
        const { tap, app, app_data, show_notification } = message;
        console.log(`message self.isActive: ${JSON.stringify(message)}, ${JSON.stringify(self.isActive)}`);

        if (tap === "background" || (show_notification === "true" && !self.isActive)) {
          console.log(`message self.isActive: ${JSON.stringify(message)}, ${JSON.stringify(self.isActive)}`);
          // 백그라운드 상태일 때 탭으로 들어온 경우
          if (app === "jgw") {
            window.gk337.connectJGW(app_data);
          } else {
            window.localStorage.setItem("lastUsedApp", "box337");
            self.hideLoadingScreen();
          }
        } else if (tap === "foreground" || (show_notification === "false" && self.isActive)) {
          try {
            console.log(`message self.isActive: ${JSON.stringify(message)}, ${JSON.stringify(self.isActive)}`);
            const { notification_body, notification_title } = message;
            // self.hideLoadingScreen();
            if (app === "jgw") {
              console.log("alertDialog start");
              const alertDialog = new AlertDialog(notification_title, notification_body, "바로가기");
              alertDialog.onClick = function (event) {
                if (event === "background") {
                  return false;
                } else if (event === "ok") {
                  if (window.location && window.location.href && window.location.href.endsWith("jgw/index.html")) {
                    const pushData = JSON.parse(app_data);
                    navigate(pushData.url, { ...pushData.param });
                  } else {
                    window.gk337.connectJGW(app_data);
                  }
                } else {
                  window.localStorage.setItem("lastUsedApp", "box337");
                }
              };
              alertDialog.show();
              console.log("alertDialog end");
            }
          } catch (err) {
            console.error(err);
          }
        }
      },
      function (error) {
        console.error(error);
      }
    );

    // 마지막 앱에 따라 분기처리
    // 마지막 앱 처리
    const lastUsedApp = window.localStorage.getItem("lastUsedApp");
    if (lastUsedApp === "jgw") {
      const { pathname } = window.location;
      if (pathname.indexOf("/jgw/") === -1) {
        window.gk337.connectJGW();
      }
    } else {
      this.hideLoadingScreen();
    }

    // Link Event
    // link/launch
    universalLinks.subscribe(null, function (eventData) {
      console.log("eventData", JSON.stringify(eventData));
      const hash = eventData.params.hash;
      const id = eventData.params.id;
      localStorage.setItem("hash", hash);
      localStorage.setItem("loginid", id);
      localStorage.setItem("login", true);
      localStorage.setItem("loginRedirectApp", "jgw");
      localStorage.setItem("launch", "gmidas");
      console.log("launch", localStorage.getItem("launch"));
      window.gk337.connectJGW();
    });

    //그냥 켰을 경우
    setTimeout(function () {
      self.hideLoadingScreen();
    }, 600);
  },
  onDeviceResume: function () {
    this.isActive = true;
    const self = this;

    // 노티 클릭으로 들어왔을때 화면 분기처리 로딩 표시
    this.showLoadingScreen();
    setTimeout(function () {
      self.hideLoadingScreen();
    }, 400);
  },
  onDevicePause: function () {
    this.isActive = false;
  },
};

common.initialize();

// class ImagePickerCamera {
//   constructor() {
//     this.onSelect = null;
//     this.onError = null;
//     this.modal_ = null;
//     this.modalBackground_ = null;
//   }

//   handleSelect(imageUrl) {
//     this.onSelect && this.onSelect(imageUrl);
//   }

//   setOptions(srcType) {
//     var options = {
//       // Some common settings are 20, 50, and 100
//       // quality: 50,
//       destinationType: Camera.DestinationType.FILE_URI,
//       // In this app, dynamically set the picture source, Camera or photo gallery
//       sourceType: srcType,
//       // encodingType: Camera.EncodingType.JPEG,
//       mediaType: Camera.MediaType.PICTURE,
//       //                allowEdit: true,
//       correctOrientation: true, //Corrects Android orientation quirks
//     };
//     return options;
//   }

//   // createNewFileEntry(imgUri) {
//   //   window.resolveLocalFileSystemURL(
//   //     cordova.file.cacheDirectory,
//   //     function success(dirEntry) {
//   //       // JPEG file
//   //       dirEntry.getFile(
//   //         "tempFile.jpeg",
//   //         {
//   //           create: true,
//   //           exclusive: false
//   //         },
//   //         function(fileEntry) {
//   //           // Do something with it, like write to it, upload it, etc.
//   //           // writeFile(fileEntry, imgUri);
//   //           console.log("got file: " + fileEntry.fullPath);
//   //           // displayFileData(fileEntry.fullPath, "File copied to");
//   //         },
//   //         onErrorCreateFile
//   //       );
//   //     },
//   //     onErrorResolveUrl
//   //   );
//   // }

//   // getFileEntry(imgUri) {
//   //   const self = this;
//   //   window.resolveLocalFileSystemURL(
//   //     imgUri,
//   //     function success(fileEntry) {
//   //       // Do something with the FileEntry object, like write to it, upload it, etc.
//   //       // writeFile(fileEntry, imgUri);
//   //       console.log("got file: " + fileEntry.fullPath);
//   //       // displayFileData(fileEntry.nativeURL, "Native URL");
//   //     },
//   //     function() {
//   //       // If don't get the FileEntry (which may happen when testing
//   //       // on some emulators), copy to a new FileEntry.
//   //       self.createNewFileEntry(imgUri);
//   //     }
//   //   );
//   // }

//   openFilePicker(imgWidth, imgHeight) {
//     console.log("ImagePickerCamera -> openFilePicker -> openFilePicker");
//     console.log(
//       "ImagePickerCamera -> openFilePicker -> imgWidth, imgHeight",
//       imgWidth,
//       imgHeight
//     );
//     const srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
//     const options = this.setOptions(srcType);
//     // var func = this.createNewFileEntry;

//     if (imgHeight) {
//       options.targetHeight = imgHeight;
//     }
//     if (imgWidth) {
//       options.targetWidth = imgWidth;
//     }
//     // if (isThumbnail) {
//     //     // To downscale a selected image,
//     //     // Camera.EncodingType (e.g., JPEG) must match the selected image type.
//     //     options.targetHeight = 100;
//     //     options.targetWidth = 100;
//     // }
//     const self = this;

//     navigator.camera.getPicture(
//       function cameraSuccess(imageUri) {
//         self.handleSelect(imageUri);
//       },
//       function cameraError(error) {
//         self.onError && self.onError(error);
//       },
//       options
//     );
//   }

//   openCamera(imgWidth, imgHeight) {
//     var srcType = Camera.PictureSourceType.CAMERA;
//     var options = this.setOptions(srcType);
//     // var func = this.createNewFileEntry;

//     if (imgHeight) {
//       options.targetHeight = imgHeight;
//     }
//     if (imgWidth) {
//       options.targetWidth = imgWidth;
//     }
//     const self = this;

//     navigator.camera.getPicture(
//       function cameraSuccess(imageUri) {
//         self.handleSelect(imageUri);
//       },
//       function cameraError(error) {
//         self.onError && self.onError(error);
//       },
//       options
//     );
//   }

//   /**
//    * 모달 팝업 클릭 이벤트 핸들러
//    * @param {string} close, picture, camera
//    */
//   handleClick(event) {
//     const self = this;
//     return function () {
//       console.log("ImagePickerCamera -> event", event);
//       let isClose = true;

//       if (self.onClick) {
//         const result = self.onClick(event);
//         if (result === false) {
//           isClose = false;
//         }
//       }

//       switch (event) {
//         case "picture": {
//           self.openFilePicker();
//           break;
//         }
//         case "camera": {
//           console.log("openCamera");
//           self.openCamera();
//           break;
//         }
//       }

//       if (isClose) {
//         self.modal_.remove();
//         self.modalBackground_.remove();
//       }
//     };
//   }

//   showPopup(imgWidth, imgHeight) {
//     const modalBackground = document.createElement("div");
//     this.modalBackground_ = modalBackground;
//     modalBackground.style.cssText = `display: block; animation: dim-on .5s; z-index: 1001; position: absolute; top: 0; right: 0; left: 0; bottom: 0; background: rgba(0, 0, 0, 0.5); margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;`;
//     modalBackground.onclick = this.handleClick("background");

//     const body = document.querySelector("body");
//     body.appendChild(modalBackground);

//     const container = document.createElement("div");
//     this.modal_ = container;
//     container.style.cssText = `transform: translateY(0); padding: 0 14px; background-color: #fff; position: absolute; bottom: 0; width: 100%; z-index: 1002; font-size: inherit; margin: 0; word-break: keep-all; list-style: none; line-height: 1em; box-sizing: border-box; transition: 0.45s cubic-bezier(0.32, 1, 0.23, 1) 0.1s;`;
//     body.appendChild(container);

//     const header = document.createElement("div");
//     header.style.cssText = `height: 54px; display: flex; justify-content: space-between; align-items: center; flex-direction: row; font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626`;
//     header.innerHTML = `<span is="c-span" class="text title" style="font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif; color: #262626">작업선택</span>`;

//     const closeButton = document.createElement("a");
//     closeButton.setAttribute("href", "javascript:void(0)");
//     closeButton.style.cssText = `margin-left: auto; text-indent: 100%; white-space: nowrap; overflow: hidden; display: inline-block; background: url(../jgw/images/icon/close-large.png) no-repeat; width: 28px; height: 28px; background-size: 28px 28px; text-decoration: none; color: inherit; font-size: inherit; padding: 0; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
//     closeButton.innerHTML = `<span is="c-span">닫기</span>`;
//     closeButton.onclick = this.handleClick("close");
//     header.appendChild(closeButton);

//     container.appendChild(header);

//     const buttonContainer = document.createElement("div");
//     buttonContainer.style.cssText = `height: 120px; display: flex; font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
//     container.appendChild(buttonContainer);

//     const buttonPicture = document.createElement("a");
//     buttonPicture.setAttribute("href", "javascript:void(0)");
//     buttonPicture.style.cssText = `text-align: center; font-size: 12px; color: #898989; margin: 0 15px; display: block; height: 70px; text-decoration: none;    padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif;`;
//     buttonPicture.innerHTML = `<img src="../jgw/images/icon/picture.png" style="width: 44px; height: 44px; display: block; margin-bottom: 10px;object-fit: cover;border: 0; vertical-align: middle;">
//     <span is="c-span" style=" font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;    word-break: keep-all;color: #898989;text-align: center;font-family: 'NotoSans-Medium', sans-serif;">사진</span>`;
//     buttonPicture.onclick = this.handleClick("picture");

//     buttonContainer.appendChild(buttonPicture);

//     const buttonCamera = document.createElement("a");
//     buttonCamera.setAttribute("href", "javascript:void(0)");
//     buttonCamera.style.cssText = `text-align: center; font-size: 12px; color: #898989; margin: 0 15px; display: block; height: 70px; text-decoration: none;    padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif;`;
//     buttonCamera.innerHTML = `<img src="../jgw/images/icon/camera-p-r.png" style="width: 44px; height: 44px; display: block; margin-bottom: 10px;object-fit: cover;border: 0; vertical-align: middle;">
//     <span is="c-span" style=" font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;    word-break: keep-all;color: #898989;text-align: center;font-family: 'NotoSans-Medium', sans-serif;">카메라</span>`;
//     buttonCamera.onclick = this.handleClick("camera");
//     buttonContainer.appendChild(buttonCamera);
//   }
// }

class AlertDialog {
  constructor(title, message, buttonLabel, options = { closeBtn: true }) {
    this.title = title;
    this.message = message;
    this.buttonLabel = buttonLabel;
    this.closeBtn = options.closeBtn;
    this.modal_ = null;
    this.onClick = null;
  }

  // registerAnimation = () => {
  //   const animation = document.createElement("style");
  //   animation.textContent = `
  //   @-webkit-keyframes animatetop {
  //     from {top:-300px; opacity:0}
  //     to {top:0; opacity:1}
  //   }
  //   @keyframes animatetop {
  //     from {top:-300px; opacity:0}
  //     to {top:0; opacity:1}
  //   }
  //  }`;
  //   document.head.appendChild(animation);
  // };

  handleClick(event) {
    const self = this;
    return function () {
      let isClose = true;

      if (self.onClick) {
        const result = self.onClick(event);
        if (result === false) {
          isClose = false;
        }
      }
      if (isClose) {
        self.modal_.remove();
        self.modalBackground_.remove();
      }
    };
  }

  show() {
    const appDir = window.cordova ? cordova.file.applicationDirectory : "/";
    const body = document.querySelector("body");
    const modalBackground = document.createElement("div");
    this.modalBackground_ = modalBackground;
    modalBackground.style.cssText = `display: block; animation: dim-on .5s; z-index: 1000010; position: absolute; top: 0; right: 0; left: 0; bottom: 0; background: rgba(0, 0, 0, 0.5); margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;`;
    modalBackground.onclick = this.handleClick("background");
    body.appendChild(modalBackground);

    const modal = document.createElement("div");
    this.modal_ = modal;
    modal.style.cssText = `display: flex; padding: 14px; background-color: #fff; border-radius: 4px; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%); z-index: 1000020; width: 300px; flex-direction: column; max-height: 400px; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;

    const modalHeader = document.createElement("div");
    modalHeader.style.cssText = `height: 20px; flex: none; display: flex; font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
    modal.appendChild(modalHeader);

    if (this.closeBtn) {
      const closeButton = document.createElement("a");
      closeButton.setAttribute("href", "javascript:void(0)");
      closeButton.style.cssText = `margin-left: auto; text-indent: 100%; white-space: nowrap; overflow: hidden; display: inline-block; background: url(${appDir}www/html/jgw/images/icon/close-large.png) no-repeat; width: 28px; height: 28px; background-size: 28px 28px; text-decoration: none; color: inherit; font-size: inherit; padding: 0; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
      closeButton.innerHTML = `<span is="c-span">닫기</span>`;
      closeButton.onclick = this.handleClick("close");
      modalHeader.appendChild(closeButton);
    }

    const modalBody = document.createElement("div");
    modalBody.style.cssText = `overflow-y: auto;font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;word-break: keep-all;`;
    modal.appendChild(modalBody);

    const modalTitle = document.createElement("h2");
    modalTitle.style.cssText = `font-size: 20px; color: #415ec4; margin-bottom: 20px; text-align: center;`;
    modalTitle.innerText = this.title;
    modalBody.appendChild(modalTitle);

    const modalContent = document.createElement("p");
    modalContent.style.cssText = `font-size: 14px; line-height: 1.57; text-align: center; margin: 0; padding: 0; list-style: none; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
    const nomarlizedMessage = this.message.replace(/\r\n/g, "<br />");
    modalContent.innerText = nomarlizedMessage;
    modalBody.appendChild(modalContent);

    const modalFooter = document.createElement("div");
    modalFooter.style.cssText = `height: 70px; padding: 30px 11px 14px; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
    modal.appendChild(modalFooter);

    const modalFooterButton = document.createElement("a");
    modalFooterButton.setAttribute("href", "javascript:void(0)");
    modalFooterButton.style.cssText = `background-color: #415ec4; height: 40px; display: flex; justify-content: center; align-items: center; flex-direction: row; padding: 0 12px; border-radius: 4px; text-decoration: none; color: inherit; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
    modalFooterButton.innerHTML = ` <span
    is="c-span"
    style="font-size: 14px; color: #fff;     margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif;"
    >${this.buttonLabel}</span>`;
    modalFooterButton.onclick = this.handleClick("ok");

    const modalFooterButton2 = document.createElement("a");
    modalFooterButton.setAttribute("href", "javascript:void(0)");
    modalFooterButton.style.cssText = `background-color: #415ec4; height: 40px; display: flex; justify-content: center; align-items: center; flex-direction: row; padding: 0 12px; border-radius: 4px; text-decoration: none; color: inherit; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
    modalFooterButton.innerHTML = ` <span
    is="c-span"
    style="font-size: 14px; color: #fff;     margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif;"
    >${this.buttonLabel}</span>`;
    modalFooterButton.onclick = this.handleClick("ok");

    modalFooter.appendChild(modalFooterButton);
    modalFooter.appendChild(modalFooterButton2);

    body.insertBefore(modal, body.firstChild);
  }
}

class HtmlDialog {
  constructor(title, message, buttonLabel, options = { closeBtn: true, onCheck: () => {} }) {
    this.title = title;
    this.message = message;
    this.buttonLabel = buttonLabel;
    this.closeBtn = options.closeBtn;
    this.modal_ = null;
    this.onClick = null;
    this.onCheck = options.onCheck;
  }

  // registerAnimation = () => {
  //   const animation = document.createElement("style");
  //   animation.textContent = `
  //   @-webkit-keyframes animatetop {
  //     from {top:-300px; opacity:0}
  //     to {top:0; opacity:1}
  //   }
  //   @keyframes animatetop {
  //     from {top:-300px; opacity:0}
  //     to {top:0; opacity:1}
  //   }
  //  }`;
  //   document.head.appendChild(animation);
  // };

  handleClick(event) {
    const self = this;
    return function () {
      let isClose = true;

      if (self.onClick) {
        const result = self.onClick(event);
        if (result === false) {
          isClose = false;
        }
      }
      if (isClose) {
        self.modal_.remove();
        self.modalBackground_.remove();
      }
    };
  }

  handleCheck(event) {
    const self = this;
    return function () {
      let isClose = true;

      if (self.onCheck) {
        self.onCheck(event);
      }
      if (isClose) {
        self.modal_.remove();
        self.modalBackground_.remove();
      }
    };
  }

  show() {
    const appDir = window.cordova ? cordova.file.applicationDirectory : "/";
    const body = document.querySelector("body");
    const modalBackground = document.createElement("div");
    this.modalBackground_ = modalBackground;
    modalBackground.style.cssText = `display: block; animation: dim-on .5s; z-index: 1000010; position: absolute; top: 0; right: 0; left: 0; bottom: 0; background: rgba(0, 0, 0, 0.5); margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;`;
    modalBackground.onclick = this.handleClick("background");
    body.appendChild(modalBackground);

    const modal = document.createElement("div");
    this.modal_ = modal;
    modal.style.cssText = `display: flex; padding: 14px; background-color: #fff; border-radius: 4px; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%); z-index: 1000020; width: 380px; flex-direction: column; max-height: 500px; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;

    const modalHeader = document.createElement("div");
    modalHeader.style.cssText = `height: 5px; flex: none; display: flex; font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
    modal.appendChild(modalHeader);

    const modalBody = document.createElement("div");
    modalBody.style.cssText = `overflow-y: auto;font-size: inherit; margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box;word-break: keep-all;`;

    // const modalBodyInnerHtml = document.createElement("div");
    // modalBodyInnerHtml.insertAdjacentHTML("beforeend", this.message);
    // modalBody.appendChild(modalBodyInnerHtml);
    modal.appendChild(modalBody);

    const modalTitle = document.createElement("h2");
    modalTitle.style.cssText = `font-size: 20px; color: #415ec4; margin-bottom: 20px; text-align: center;`;
    modalTitle.innerText = this.title;
    if (this.closeBtn) {
      const closeButton = document.createElement("a");
      closeButton.setAttribute("href", "javascript:void(0)");
      closeButton.style.cssText = `position: absolute; left: 90%; top: 2%; margin-left: auto; text-indent: 100%; white-space: nowrap; overflow: hidden; display: inline-block; background: url(${appDir}www/html/jgw/images/icon/close-large.png) no-repeat; width: 28px; height: 28px; background-size: 28px 28px; text-decoration: none; color: inherit; font-size: inherit; padding: 0; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
      closeButton.innerHTML = `<span is="c-span">닫기</span>`;
      closeButton.onclick = this.handleClick("close");
      modalBody.appendChild(closeButton);
    }
    modalBody.appendChild(modalTitle);

    const modalContent = document.createElement("p");
    modalContent.style.cssText = `font-size: 14px; line-height: 1.57; margin: 0; padding: 0; list-style: none; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
    const nomarlizedMessage = this.message;
    modalContent.innerHTML = nomarlizedMessage;
    modalBody.appendChild(modalContent);

    const modalFooter = document.createElement("div");
    modalFooter.style.cssText = `height: 70px; padding: 30px 11px 14px; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif; color: #262626;`;
    modal.appendChild(modalFooter);

    const doNotShowAgain = document.createElement("div");
    doNotShowAgain.innerHTML =
      "<label style='visibility: visible;'><input type='checkbox' class='shape-7'/><span class='text'>다시보지 않기</span></label>";

    doNotShowAgain.onclick = this.handleCheck();
    modalFooter.appendChild(doNotShowAgain);

    // const modalFooterButton = document.createElement("a");
    // modalFooterButton.setAttribute("href", "javascript:void(0)");
    // modalFooterButton.style.cssText = `background-color: #415ec4; height: 40px; display: flex; justify-content: center; align-items: center; flex-direction: row; padding: 0 12px; border-radius: 4px; text-decoration: none; color: inherit; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
    // modalFooterButton.innerHTML = ` <span
    // is="c-span"
    // style="font-size: 14px; color: #fff;     margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif;"
    // >${this.buttonLabel}</span>`;
    // modalFooterButton.onclick = this.handleClick("ok");
    //
    // const modalFooterButton2 = document.createElement("a");
    // modalFooterButton.setAttribute("href", "javascript:void(0)");
    // modalFooterButton.style.cssText = `background-color: #415ec4; height: 40px; display: flex; justify-content: center; align-items: center; flex-direction: row; padding: 0 12px; border-radius: 4px; text-decoration: none; color: inherit; font-size: inherit; margin: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all; font-family: 'NotoSans-Medium', sans-serif;`;
    // modalFooterButton.innerHTML = ` <span
    // is="c-span"
    // style="font-size: 14px; color: #fff;     margin: 0; padding: 0; list-style: none; line-height: 1em; box-sizing: border-box; word-break: keep-all;font-family: 'NotoSans-Medium', sans-serif;"
    // >${this.buttonLabel}</span>`;
    // modalFooterButton.onclick = this.handleClick("ok");
    //
    // modalFooter.appendChild(modalFooterButton);
    // modalFooter.appendChild(modalFooterButton2);

    body.insertBefore(modal, body.firstChild);
  }
}
