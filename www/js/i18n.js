(function () {
  let langMaps = null;
  let deviceLoaded = false;
  let c = false;
  document.addEventListener(
    "deviceready",
    () => {
      deviceLoaded = true;
    },
    false
  );
  if (jQuery.i18n) {
    jQuery.i18n.prop = (name) => {
      if (langMaps !== null) {
        return langMaps.get(name.trim());
      }
      return name;
    };
  }
  if (jQuery.i18n) {
    jQuery.i18n.properties = ({ language, callback }) => {
      const core = () => {
        if (langMaps === null) {
          let url = null;
          if (language === "en") {
            url = "https://app.gk337.co.kr/www/messages/messages_en.properties";
          }
          if (language === "ja") {
            url = "https://app.gk337.co.kr/www/messages/messages_ja.properties";
          }
          if (url === null) {
            moduleReady = true;
            return;
          }
          fetch(url)
            .then((r) => r.text())
            .then((data) => {
              const map = new Map();
              const keyAndValueArray = data.split("\n").filter((c) => c.trim().length > 0);
              for (let i = 0; i < keyAndValueArray.length; i++) {
                const keyAndValue = keyAndValueArray[i];
                const key = keyAndValue.split("=")[0];
                const value = keyAndValue.split("=")[1];
                if (key !== undefined && value !== undefined) {
                  map.set(key.trim(), value.trim());
                }
              }
              langMaps = map;
              moduleReady = true;
              callback();
            });
        } else {
          moduleReady = true;
          callback();
        }
      };
      if (deviceLoaded) {
        core();
      } else {
        setTimeout(() => {
          jQuery.i18n.properties({ language, callback });
        }, 100);
      }
    };
  }
})();
