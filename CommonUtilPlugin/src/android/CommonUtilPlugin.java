package com.gibubox.CommonUtilPlugin;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * This class echoes a string called from JavaScript.
 */
public class CommonUtilPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("toast")) {
            String message = args.getString(0);

            Toast.makeText(cordova.getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            if (message != null && message.length() > 0) {
                callbackContext.success("Toast Text : "+message);
            } else {
                callbackContext.error("Expected one non-empty string argument.");
            }
            return true;
        }
        return false;

        if (action.equals("save_token")) {
            String id = args.getString(0);
            Context mContext = this.cordova.getActivity().getApplicationContext();
            String token = Utils.getIns(mContext).getPref(Utils.PUSH_TOKEM);

            //토큰이 있으면 서버에 저장
            if(!token.isEmpty()) {
                NetST.getIns().initBaseUrl();
                RetrofitExService retrofitExService = NetST.getIns().getService(RetrofitExService.class);
                HashMap<String, Object> input = new HashMap<>();
                input.put("id", id);
                input.put("token", token);
                retrofitExService.postData(input).enqueue(new Callback<Data>() {
                    @Override
                    public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                        if (response.isSuccessful()) {
                            Data body = response.body();
                            if (body != null) {
                                Log.d("CommonUtilPlugin", "data" + new Gson().toJson(response.body()));
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {

                    }
                });
            }

            if (id != null || token != null) {
                callbackContext.success(token);
            } else {
                callbackContext.error("saveToken fail");
            }
            return true;
        }
    }
}
