var exec = require("cordova/exec");

exports.toast = function (arg0, success, error) {
  exec(success, error, "CommonUtilPlugin", "toast", [arg0]);
};

exports.save_token = function (arg0, success, error) {
  exec(success, error, "CommonUtilPlugin", "save_token", [arg0]);
};
