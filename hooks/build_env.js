#!/usr/bin/env node

// console.warn(process.argv);
module.exports = function (ctx) {
  // console.warn("ctx : ", ctx);
  var wwwFileToReplace = "js/env.js";

  var fs = require("fs");
  var path = require("path");

  var rootdir = process.argv[2];

  function loadConfigXMLDoc(filePath) {
    var fs = require("fs");
    var xml2js = require("xml2js");
    var json = "";
    try {
      var fileData = fs.readFileSync(filePath, "utf-8");
      var parser = new xml2js.Parser();
      parser.parseString(fileData.substring(0, fileData.length), function (err, result) {
        //console.log("config.xml as JSON", JSON.stringify(result, null, 2));
        json = result;
      });
      console.log("File '" + filePath + "' was successfully read.");
      return json;
    } catch (ex) {
      console.log(ex);
    }
  }

  function replace_string_in_file(filename, to_replace, replace_with) {
    var data = fs.readFileSync(filename, "utf8");

    //console.warn("to_replace : %s, replace_with : %s", to_replace, replace_with);
    var result = data.replace(new RegExp(to_replace, "gim"), replace_with);
    // var result = data.replace(/__VERSION__/gim, version);
    //console.warn("replace", result);
    fs.writeFileSync(filename, result, "utf8");
  }

  var configXMLPath = "config.xml";
  var rawJSON = loadConfigXMLDoc(configXMLPath);
  var version = rawJSON.widget.$.version;
  console.log("Version:", version);

  var buildPlatform = process.argv[3];
  var profile = process.env.npm_config_profile || process.env.NODE_ENV || "PRD";
  var knoxYn = process.env.npm_config_knoxYn || process.env.KNOX_YN || "N";

  console.info(`[BUILD_ENV] build profile : ${profile}`);
  console.info(`[BUILD_ENV] platform : ${buildPlatform}`);
  console.info(`[BUILD_ENV] version - ${version}`);
  console.info(`[BUILD_ENV] knox - ${knoxYn}`);

  if (buildPlatform) {
    var wwwPath = "";
    switch (buildPlatform) {
      case "ios":
        wwwPath = "platforms/ios/www/";
        break;
      case "android":
        // wwwPath = "platforms/android/app/src/main/assets/www/";
        // TODO : asset 폴더 직접 접근
        wwwPath = "www/";
        break;
      default:
        console.log("Unknown build platform: " + buildPlatform);
    }
    var fullfilename = path.join(wwwPath + wwwFileToReplace);
    // console.info("fullfilename : ", fullfilename);
    if (fs.existsSync(fullfilename)) {
      // VERSION
      replace_string_in_file(fullfilename, 'var\\s+_version.*"', `var _version = "${version}"`);
      // MODE
      replace_string_in_file(fullfilename, 'var\\s+_mode.*"', `var _mode = "${profile}"`);
      // PLATFORM
      replace_string_in_file(fullfilename, 'var\\s+_platform.*"', `var _platform = "${buildPlatform}"`);
      // KNOX_YN
      replace_string_in_file(fullfilename, 'var\\s+_knox_yn.*"', `var _knox_yn = "${knoxYn}"`);
      console.log("Replaced env in file: " + fullfilename);
    }
  }
};
